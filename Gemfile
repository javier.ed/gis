# frozen_string_literal: true

source 'https://rubygems.org'

git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '~> 2.7.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0', '>= 6.0.3.3'
# Use Puma as the app server
gem 'puma', '~> 5.0', '>= 5.0.2'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.8'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 4.2', '>= 4.2.2'
# Use Active Model has_secure_password
gem 'bcrypt', '~> 3.1', '>= 3.1.16'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '~> 1.4', '>= 1.4.8', require: false

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors', '~> 1.1', '>= 1.1.1'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'factory_bot_rails', '~> 6.1'
  gem 'i18n-tasks', '~> 0.9.31'
  gem 'rspec-rails', '~> 4.0', '>= 4.0.1'
  gem 'rubocop', '~> 0.92.0'
  gem 'rubocop-i18n', '~> 2.0', '>= 2.0.2'
  gem 'rubocop-performance', '~> 1.8', '>= 1.8.1'
  gem 'rubocop-rails', '~> 2.8', '>= 2.8.1'
  gem 'rubocop-rspec', '~> 1.43', '>= 1.43.2'
end

group :development do
  gem 'listen', '~> 3.2', '>= 3.2.1'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0', '>= 2.0.1'
end

group :test do
  gem 'database_cleaner-active_record', '~> 1.8'
  gem 'database_cleaner-redis', '~> 1.8'
  gem 'email_spec', '~> 2.2'
  gem 'rails-controller-testing', '~> 1.0', '>= 1.0.5'
  gem 'shoulda-matchers', '~> 4.4', '>= 4.4.1'
  gem 'simplecov', '~> 0.19.0', require: false
  gem 'webmock', '~> 3.9', '>= 3.9.1'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

# Database management
gem 'paper_trail', '~> 11.0'
gem 'paper_trail-association_tracking', '~> 2.1'
gem 'pg', '~> 1.2', '>= 1.2.3'

# Authentication
gem 'jwt', '~> 2.2', '>= 2.2.2'

# Localization
gem 'countries', '~> 3.0', '>= 3.0.1'
gem 'globalize', '~> 5.3'
gem 'globalize-versioning', '~> 0.4.0'
gem 'rails-i18n', '~> 6.0'
gem 'timeliness-i18n', '~> 0.9.0'

# Validators
gem 'active_storage_validations', '~> 0.9.0'
gem 'validates_timeliness', '~> 5.0.0.beta2'

# GraphQL
gem 'apollo_upload_server', '~> 2.0', '>= 2.0.3'
gem 'graphql', '~> 1.11', '>= 1.11.4'

# Background jobs
gem 'sidekiq', '~> 6.1', '>= 6.1.2'
gem 'sidekiq-cron', '~> 1.2'

# JSON
gem 'json', '~> 2.3', '>= 2.3.1'

# GIS
gem 'activerecord-postgis-adapter', '~> 6.0', '>= 6.0.1'
gem 'geocoder', '~> 1.6', '>= 1.6.3'

# Storage
gem 'image_processing', '~> 1.12'
gem 'mini_magick', '~> 4.10', '>= 4.10.1'

# Utils
gem 'addressable', '~> 2.7'
gem 'gitlab_chronic_duration', '~> 0.10.6.2'
