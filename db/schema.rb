# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_08_18_202112) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "postgis"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "client_apps", force: :cascade do |t|
    t.string "description", limit: 256, null: false
    t.string "scopes", limit: 256, null: false
    t.string "key", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["key"], name: "index_client_apps_on_key", unique: true
  end

  create_table "favorite_markers", force: :cascade do |t|
    t.bigint "user_id", default: 0, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "marker_id", default: 0, null: false
    t.index ["marker_id"], name: "index_favorite_markers_on_marker_id"
    t.index ["user_id", "marker_id"], name: "index_favorite_markers_on_user_id_and_marker_id", unique: true
    t.index ["user_id"], name: "index_favorite_markers_on_user_id"
  end

  create_table "geocoding_point_translations", force: :cascade do |t|
    t.bigint "geocoding_point_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "address", null: false
    t.index ["geocoding_point_id"], name: "index_geocoding_point_translations_on_geocoding_point_id"
    t.index ["locale"], name: "index_geocoding_point_translations_on_locale"
  end

  create_table "geocoding_points", force: :cascade do |t|
    t.float "latitude", null: false
    t.float "longitude", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.geography "coordinates", limit: {:srid=>4326, :type=>"st_point", :geographic=>true}
    t.index ["latitude", "longitude"], name: "index_geocoding_points_on_latitude_and_longitude", unique: true
  end

  create_table "layer_field_option_translations", force: :cascade do |t|
    t.bigint "layer_field_option_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "name", limit: 256, null: false
    t.index ["layer_field_option_id"], name: "index_layer_field_option_translations_on_layer_field_option_id"
    t.index ["locale"], name: "index_layer_field_option_translations_on_locale"
  end

  create_table "layer_field_options", force: :cascade do |t|
    t.bigint "layer_field_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["layer_field_id"], name: "index_layer_field_options_on_layer_field_id"
  end

  create_table "layer_field_translations", force: :cascade do |t|
    t.bigint "layer_field_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "name", limit: 256, null: false
    t.text "description"
    t.index ["layer_field_id"], name: "index_layer_field_translations_on_layer_field_id"
    t.index ["locale"], name: "index_layer_field_translations_on_locale"
  end

  create_table "layer_fields", force: :cascade do |t|
    t.string "field_type", null: false
    t.boolean "required"
    t.boolean "unique"
    t.boolean "editable"
    t.bigint "layer_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["layer_id"], name: "index_layer_fields_on_layer_id"
  end

  create_table "layer_subscriptions", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "layer_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["layer_id"], name: "index_layer_subscriptions_on_layer_id"
    t.index ["user_id", "layer_id"], name: "index_layer_subscriptions_on_user_id_and_layer_id", unique: true
    t.index ["user_id"], name: "index_layer_subscriptions_on_user_id"
  end

  create_table "layer_translations", force: :cascade do |t|
    t.bigint "layer_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "name", limit: 256, null: false
    t.text "description", null: false
    t.index ["layer_id"], name: "index_layer_translations_on_layer_id"
    t.index ["locale"], name: "index_layer_translations_on_locale"
  end

  create_table "layers", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "time_interval", default: "any", null: false
    t.boolean "start_at_current_time"
    t.string "suggestions", default: "requires_approval", null: false
    t.index ["user_id"], name: "index_layers_on_user_id"
  end

  create_table "marker_approvals", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "marker_id", null: false
    t.boolean "approved"
    t.text "comment"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["marker_id"], name: "index_marker_approvals_on_marker_id"
    t.index ["user_id"], name: "index_marker_approvals_on_user_id"
  end

  create_table "marker_translations", force: :cascade do |t|
    t.bigint "marker_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "name", limit: 256, null: false
    t.text "description"
    t.index ["locale"], name: "index_marker_translations_on_locale"
    t.index ["marker_id"], name: "index_marker_translations_on_marker_id"
  end

  create_table "marker_value_translations", force: :cascade do |t|
    t.bigint "marker_value_id", null: false
    t.string "locale", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "string_value"
    t.text "text_value"
    t.index ["locale"], name: "index_marker_value_translations_on_locale"
    t.index ["marker_value_id"], name: "index_marker_value_translations_on_marker_value_id"
  end

  create_table "marker_values", force: :cascade do |t|
    t.bigint "integer_value"
    t.float "float_value"
    t.boolean "boolean_value"
    t.datetime "datetime_value"
    t.bigint "layer_field_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.date "date_value"
    t.time "time_value"
    t.bigint "layer_field_option_id"
    t.bigint "marker_id", null: false
    t.index ["layer_field_id"], name: "index_marker_values_on_layer_field_id"
    t.index ["layer_field_option_id"], name: "index_marker_values_on_layer_field_option_id"
    t.index ["marker_id", "layer_field_id"], name: "index_marker_values_on_marker_id_and_layer_field_id", unique: true
    t.index ["marker_id"], name: "index_marker_values_on_marker_id"
  end

  create_table "markers", force: :cascade do |t|
    t.bigint "layer_id", null: false
    t.bigint "geocoding_point_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.datetime "published_at"
    t.bigint "marker_approval_id"
    t.datetime "start_at"
    t.datetime "finish_at"
    t.index ["geocoding_point_id"], name: "index_markers_on_geocoding_point_id"
    t.index ["layer_id"], name: "index_markers_on_layer_id"
    t.index ["marker_approval_id"], name: "index_markers_on_marker_approval_id"
    t.index ["user_id"], name: "index_markers_on_user_id"
  end

  create_table "otps", force: :cascade do |t|
    t.string "action", null: false
    t.string "password_digest", null: false
    t.datetime "used_at"
    t.datetime "discarded_at"
    t.integer "attempts", default: 0, null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_otps_on_user_id"
  end

  create_table "profiles", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "name", limit: 256
    t.text "bio"
    t.string "country_id"
    t.date "birthdate"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_profiles_on_user_id", unique: true
  end

  create_table "sessions", force: :cascade do |t|
    t.string "key", null: false
    t.datetime "finished_at"
    t.bigint "client_app_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["client_app_id"], name: "index_sessions_on_client_app_id"
    t.index ["key"], name: "index_sessions_on_key", unique: true
    t.index ["user_id"], name: "index_sessions_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "username", limit: 24, null: false
    t.string "password_digest", null: false
    t.string "email", limit: 256, null: false
    t.datetime "email_verified_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "version_associations", force: :cascade do |t|
    t.integer "version_id"
    t.string "foreign_key_name", null: false
    t.integer "foreign_key_id"
    t.string "foreign_type"
    t.index ["foreign_key_name", "foreign_key_id", "foreign_type"], name: "index_version_associations_on_foreign_key"
    t.index ["version_id"], name: "index_version_associations_on_version_id"
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.bigint "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.text "object_changes"
    t.integer "transaction_id"
    t.string "locale"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
    t.index ["transaction_id"], name: "index_versions_on_transaction_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "favorite_markers", "markers"
  add_foreign_key "favorite_markers", "users"
  add_foreign_key "layer_field_options", "layer_fields"
  add_foreign_key "layer_fields", "layers"
  add_foreign_key "layer_subscriptions", "layers"
  add_foreign_key "layer_subscriptions", "users"
  add_foreign_key "layers", "users"
  add_foreign_key "marker_approvals", "markers"
  add_foreign_key "marker_approvals", "users"
  add_foreign_key "marker_values", "layer_field_options"
  add_foreign_key "marker_values", "layer_fields"
  add_foreign_key "marker_values", "markers"
  add_foreign_key "markers", "geocoding_points"
  add_foreign_key "markers", "layers"
  add_foreign_key "markers", "marker_approvals"
  add_foreign_key "markers", "users"
  add_foreign_key "otps", "users"
  add_foreign_key "profiles", "users"
  add_foreign_key "sessions", "client_apps"
  add_foreign_key "sessions", "users"
end
