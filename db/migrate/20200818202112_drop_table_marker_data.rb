class DropTableMarkerData < ActiveRecord::Migration[6.0]
  def change
    remove_reference :marker_values, :marker_data, foreign_key: true

    drop_table :marker_data do |t|
      t.datetime :start_at
      t.datetime :finish_at
      t.bigint :marker_id, null: false
      t.bigint :user_id, null: false

      t.timestamps
    end
  end
end
