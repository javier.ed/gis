# frozen_string_literal: true

# Create Schema
class CreateSchema < ActiveRecord::Migration[6.0]
  def change
    create_table :client_apps do |t|
      t.string :description,  null: false, limit: 256
      t.string :scopes,       null: false, limit: 256
      t.string :token,        null: false

      t.timestamps

      t.index :token, unique: true
    end

    create_table :geocoding_points do |t|
      t.string  :name,      null: false
      t.float   :latitude,  null: false
      t.float   :longitude, null: false
      t.string  :address,   null: false

      t.references :data, null: false, polymorphic: true, index: { unique: true }

      t.timestamps
    end

    create_table :geographic_data do |t|
      t.geometry :geometry

      t.timestamps
    end

    create_table :nominatim_data do |t|
      t.bigint  :place_id,    null: false
      t.string  :osm_type,    null: false
      t.bigint  :osm_id,      null: false
      t.string  :display_name
      t.bigint  :place_rank
      t.string  :category
      t.string  :type
      t.float   :importance
      t.string  :icon

      t.text  :address
      t.text  :extra_tags
      t.text  :name_details

      t.st_point  :bounding_box_min,  geographic: true
      t.st_point  :bounding_box_max,  geographic: true
      t.geometry  :geometry,          geographic: true

      t.timestamps

      t.index :place_id,            unique: true
      t.index :osm_id,              unique: true
    end

    create_table :users do |t|
      t.string    :username,                 null: false, limit: 24
      t.string    :password_digest,          null: false
      t.string    :email,                    null: false, limit: 256
      t.string    :email_verification_token
      t.datetime  :email_verified_at

      t.timestamps

      t.index :email_verification_token,  unique: true
      t.index :username,                  unique: true
      t.index :email,                     unique: true
    end

    create_table :layers do |t|
      t.string  :name,         null: false, limit: 256
      t.text    :description,  null: false

      t.references :user, null: false, foreign_key: true

      t.timestamps

      t.index :name, unique: true
    end

    create_table :layer_fields do |t|
      t.string  :type,        null: false
      t.string  :name,        null: false, limit: 256
      t.text    :description

      t.boolean :required
      t.boolean :unique
      t.boolean :readonly

      t.bigint :time_range

      t.references :layer, null: false, foreign_key: true

      t.timestamps
    end

    create_table :layer_field_options do |t|
      t.string :name, null: false, limit: 256

      t.references :layer_field, null: false, foreign_key: true

      t.timestamps
    end

    create_table :layer_cards do |t|
      t.references :layer,            null: false, foreign_key: true
      t.references :geocoding_point,  null: false, foreign_key: true
      t.references :user,             null: false, foreign_key: true

      t.timestamps

      t.index %i[layer_id geocoding_point_id], unique: true
    end

    create_table :layer_card_values do |t|
      t.string    :string_value
      t.text      :text_value
      t.bigint    :integer_value
      t.float     :float_value
      t.boolean   :boolean_value
      t.datetime  :datetime_value

      t.datetime :start_at
      t.datetime :end_at

      t.references :layer_field,  null: false, foreign_key: true
      t.references :layer_card,   null: false, foreign_key: true

      t.timestamps

      t.index %i[layer_field_id layer_card_id start_at end_at],
              name: :index_layer_card_values_on_layer_field_id_and_layer_card_id,
              unique: true
    end

    create_table :active_storage_blobs do |t|
      t.string   :key,          null: false
      t.string   :filename,     null: false
      t.string   :content_type
      t.text     :metadata
      t.bigint   :byte_size,    null: false
      t.string   :checksum,     null: false
      t.datetime :created_at,   null: false

      t.index :key, unique: true
    end

    create_table :active_storage_attachments do |t|
      t.string     :name,   null: false
      t.references :record, null: false, polymorphic: true, index: false
      t.references :blob,   null: false

      t.datetime :created_at, null: false

      t.index %i[record_type record_id name blob_id], name: 'index_active_storage_attachments_uniqueness', unique: true
      t.foreign_key :active_storage_blobs, column: :blob_id
    end
  end
end
