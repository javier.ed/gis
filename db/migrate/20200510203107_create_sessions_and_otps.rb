class CreateSessionsAndOtps < ActiveRecord::Migration[6.0]
  def change
    create_table :sessions do |t|
      t.string :key, null: false
      t.datetime :finished_at
      t.references :client_app, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true

      t.timestamps

      t.index :key, unique: true
    end

    rename_column :client_apps, :token, :key

    create_table :otps do |t|
      t.string :action, null: false
      t.string :password_digest, null: false
      t.datetime :used_at
      t.datetime :discarded_at
      t.integer :attempts, null: false, default: 0
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end

    remove_column :users, :email_verification_token, :string
  end
end
