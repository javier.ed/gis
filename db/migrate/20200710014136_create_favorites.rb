class CreateFavorites < ActiveRecord::Migration[6.0]
  def change
    create_table :favorites do |t|
      t.references :user, foreign_key: true

      t.references :favoriteable, polymorphic: true, null: false

      t.timestamps

      t.index %i[user_id favoriteable_id favoriteable_type], name: :index_favorites_on_user_id_and_favoriteable,
                                                             unique: true
    end
  end
end
