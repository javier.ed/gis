class CreateTranslations < ActiveRecord::Migration[6.0]
  def self.up
    translation_table_options = { migrate_data: true, remove_source_columns: true }

    I18n.with_locale(:en) do
      GeocodingPoint.create_translation_table!({ address: { type: :string, null: false } }, translation_table_options)

      Layer.create_translation_table!(
        { name: { type: :string, null: false, limit: 256 }, description: { type: :text, null: false } },
        translation_table_options
      )

      LayerField.create_translation_table!(
        { name: { type: :string, null: false, limit: 256 }, description: :text }, translation_table_options
      )

      LayerFieldOption.create_translation_table!(
        { name: { type: :string, null: false, limit: 256 } }, translation_table_options
      )

      Marker.create_translation_table!(
        { name: { type: :string, null: false, limit: 256 }, description: :text }, translation_table_options
      )

      MarkerValue.create_translation_table!({ string_value: :string, text_value: :text }, translation_table_options)
    end

    add_column :versions, :locale, :string
  end

  def self.down
    GeocodingPoint.drop_translation_table! migrate_data: true
    Layer.drop_translation_table! migrate_data: true
    LayerField.drop_translation_table! migrate_data: true
    LayerFieldOption.drop_translation_table! migrate_data: true
    Marker.drop_translation_table! migrate_data: true
    MarkerValue.drop_translation_table! migrate_data: true

    remove_column :versions, :locale, :string
  end
end
