class AddTimeIntervalPrivateToLayer < ActiveRecord::Migration[6.0]
  def change
    add_column :layers, :time_interval, :string, null: false, default: 'any'
    add_column :layers, :start_at_current_time, :boolean
    add_column :layers, :suggestions, :string, null: false, default: 'requires_approval'
  end
end
