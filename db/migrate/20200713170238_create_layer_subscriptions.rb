class CreateLayerSubscriptions < ActiveRecord::Migration[6.0]
  def change
    create_table :layer_subscriptions do |t|
      t.references :user, foreign_key: true
      t.references :layer, null: false, foreign_key: true

      t.timestamps

      t.index %i[user_id layer_id], unique: true
    end

    rename_table :favorites, :favorite_markers

    add_reference :favorite_markers, :marker, null: false, foreign_key: true, default: 0
    change_column :favorite_markers, :user_id, :bigint, null: false, default: 0
    remove_column :favorite_markers, :favoriteable_type, :string
    remove_column :favorite_markers, :favoriteable_id, :bigint
    add_index :favorite_markers, %i[user_id marker_id], unique: true
  end
end
