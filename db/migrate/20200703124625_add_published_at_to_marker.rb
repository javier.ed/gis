class AddPublishedAtToMarker < ActiveRecord::Migration[6.0]
  def change
    create_table :marker_approvals do |t|
      t.references :user, null: false, foreign_key: true
      t.references :marker, null: false, foreign_key: true
      t.boolean :approved
      t.text :comment

      t.timestamps
    end

    add_column :markers, :published_at, :datetime
    add_reference :markers, :marker_approval, foreign_key: true

    drop_table :approval_comments do |t|
      t.integer :request_id, null: false
      t.integer :user_id, null: false
      t.text :content, null: false

      t.timestamps
    end

    drop_table :approval_items, id: :serial, force: :cascade do |t|
      t.integer :request_id, null: false
      t.integer :resource_id
      t.string :resource_type, null: false
      t.string :event, null: false
      t.text :params

      t.timestamps
    end

    drop_table :approval_requests, id: :serial, force: :cascade do |t|
      t.integer :request_user_id, null: false
      t.integer :respond_user_id
      t.integer :state, limit: 2, default: 0, null: false
      t.datetime :requested_at, null: false
      t.datetime :cancelled_at
      t.datetime :approved_at
      t.datetime :rejected_at
      t.datetime :executed_at

      t.timestamps
    end

    Marker.joins(:layer).where('published_at IS NULL AND markers.user_id = layers.user_id').each do |marker|
      marker.update(published_at: marker.updated_at || marker.created_at || Time.current)
    end
  end
end
