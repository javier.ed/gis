class CreateProfiles < ActiveRecord::Migration[6.0]
  def change
    create_table :profiles do |t|
      t.references :user, index: { unique: true }, null: false, foreign_key: true
      t.string :name, limit: 256
      t.text :bio
      t.string :country_id
      t.date :birthdate

      t.timestamps
    end
  end
end
