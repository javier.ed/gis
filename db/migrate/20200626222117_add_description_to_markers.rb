class AddDescriptionToMarkers < ActiveRecord::Migration[6.0]
  def change
    add_column :markers, :description, :text
  end
end
