class RenameColumnReadonlyToEditableInLayerFields < ActiveRecord::Migration[6.0]
  def change
    rename_column :layer_fields, :type, :field_type
    rename_column :layer_fields, :readonly, :editable
    remove_column :layer_fields, :time_range, :bigint
  end
end
