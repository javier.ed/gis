class RenameTableMarkerDataValuesToMarkerValues < ActiveRecord::Migration[6.0]
  def change
    rename_table :marker_data_values, :marker_values
    add_reference :marker_values, :marker, foreign_key: true

    MarkerValue.all.each do |marker_value|
      next unless marker_value.marker.nil? && marker_value.marker_data.marker.present?

      marker_value.update(marker: marker_value.marker_data.marker)
    end

    reversible do |dir|
      dir.up do
        change_column :marker_values, :marker_id, :bigint, null: false
        change_column :marker_values, :marker_data_id, :bigint, null: true, default: nil
      end

      dir.down do
        change_column :marker_values, :marker_id, :bigint
        change_column :marker_values, :marker_data_id, :bigint, null: false, default: 0
      end
    end

    add_column :markers, :start_at, :datetime
    add_column :markers, :finish_at, :datetime
    add_index :marker_values, %i[marker_id layer_field_id], unique: true
    remove_index :markers, %i[name layer_id]
    remove_index :markers, %i[layer_id geocoding_point_id]
    remove_index :marker_values, %i[marker_data_id layer_field_id]
  end
end
