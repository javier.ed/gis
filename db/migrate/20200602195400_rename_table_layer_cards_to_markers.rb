class RenameTableLayerCardsToMarkers < ActiveRecord::Migration[6.0]
  def change
    remove_column :layer_card_values, :start_at, :datetime
    remove_column :layer_card_values, :end_at, :datetime
    remove_reference :layer_card_values, :layer_card, foreign_key: true

    rename_table :layer_cards, :markers
    rename_table :layer_card_values, :marker_data_values

    create_table :marker_data do |t|
      t.datetime :start_at
      t.datetime :finish_at
      t.references :marker, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true

      t.timestamps

      t.index %i[marker_id start_at finish_at], unique: true
    end

    drop_table :geographic_data do |t|
      t.geometry :geometry

      t.timestamps
    end

    add_column :markers, :name, :string, null: false, limit: 256, default: ''
    add_index :markers, %i[name layer_id], unique: true

    rename_column :nominatim_data, :type, :place_type
    add_column :nominatim_data, :lng_lat, :st_point, geographic: true

    add_column :marker_data_values, :date_value, :date
    add_column :marker_data_values, :time_value, :time
    add_reference :marker_data_values, :layer_field_option, foreign_key: true
    add_reference :marker_data_values, :marker_data, null: false, foreign_key: true, default: 0
    add_index :marker_data_values, %i[marker_data_id layer_field_id], unique: true

    remove_reference :geocoding_points, :data, polymorphic: true
    add_reference :geocoding_points, :nominatim_data, foreign_key: true
    add_index :geocoding_points, %i[latitude longitude], unique: true

    reversible do |dir|
      dir.up do
        change_column :marker_data_values, :string_value, :string, limit: 256
        change_column :geocoding_points, :name, :string, limit: 256
        change_column :geocoding_points, :address, :string, limit: 256
      end

      dir.down do
        change_column :marker_data_values, :string_value, :string, limit: nil
        change_column :geocoding_points, :name, :string, limit: nil
        change_column :geocoding_points, :address, :string, limit: nil
      end
    end
  end
end
