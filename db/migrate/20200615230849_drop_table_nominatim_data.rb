class DropTableNominatimData < ActiveRecord::Migration[6.0]
  def change
    remove_reference :geocoding_points, :nominatim_data, foreign_key: true
    remove_column :geocoding_points, :name, :string
    add_column :geocoding_points, :coordinates, :st_point, geographic: true

    drop_table :nominatim_data do |t|
      t.bigint :place_id,    null: false
      t.string :osm_type,    null: false
      t.bigint :osm_id,      null: false
      t.string :display_name
      t.bigint :place_rank
      t.string :category
      t.string :place_type
      t.float  :importance
      t.string :icon

      t.text :address
      t.text :extra_tags
      t.text :name_details

      t.st_point :lng_lat,          geographic: true
      t.st_point :bounding_box_min, geographic: true
      t.st_point :bounding_box_max, geographic: true
      t.geometry :geometry,         geographic: true

      t.timestamps

      t.index :place_id, unique: true
      t.index :osm_id,   unique: true
    end

    add_index :layer_fields, %i[name layer_id], unique: true
    add_index :layer_field_options, %i[name layer_field_id], unique: true

    GeocodingPoint.all.each(&:save)
  end
end
