# frozen_string_literal: true

require 'settings/fail2ban'
require 'settings/geocoder'
require 'settings/apertium'
require 'settings/mailer'
require 'settings/queues'

# Settings
module Settings
  def self.recursive_merge(old_hash, new_hash)
    old_hash.merge(new_hash) do |_key, old_var, new_var|
      if old_var.is_a? Hash
        recursive_merge old_var, new_var.to_h
      else
        new_var
      end
    end
  end

  private_class_method :recursive_merge

  SOURCE = recursive_merge(
    {
      'default' => {
        'host' => 'localhost',
        'port' => 3000,
        'protocol' => 'http',

        'contact_mail' => nil,

        'locales' => %w[en es],

        'username_blacklist' => %w[admin administrator bululu],

        'fail2ban' => {
          'enable' => false,
          'logfile' => 'log/fail2ban.log'
        },

        'geocoder' => {
          'redis_url' => 'redis://127.0.0.1:6379/3'
        },

        'apertium' => {
          'apy_url' => 'https://www.apertium.org/apy'
        },

        'mailer' => {
          'enable' => false,
          'sender_address' => 'no-reply@localhost',
          'method' => 'sendmail', # or smtp
          ### Only sendmail
          'location' => '/usr/sbin/sendmail',
          'exim_fix' => false,
          ### Only SMTP
          'address' => 'localhost',
          'port' => 587,
          'authentication' => 'plain',
          'username' => nil,
          'password' => nil,
          'enable_starttls_auto' => true,
          'domain' => nil,
          'openssl_verify_mode' => nil,
          'ca_file' => nil
        },

        'queues' => {
          'redis_url' => 'redis://127.0.0.1:6379/2'
        }
      }, Rails.env => {}
    }, YAML.load_file(Rails.root.join('config/settings.yml')) || {}
  )

  SETTINGS = recursive_merge(SOURCE['default'], SOURCE[Rails.env]).with_indifferent_access

  private_constant :SOURCE, :SETTINGS

  class << self
    def host
      SETTINGS[:host]
    end

    def port
      SETTINGS[:port]
    end

    def protocol
      SETTINGS[:protocol]
    end

    def contact_mail
      SETTINGS[:contact_mail]
    end

    def locales
      SETTINGS[:locales]
    end

    def username_blacklist
      SETTINGS[:username_blacklist]
    end

    def fail2ban
      Fail2an.new(SETTINGS[:fail2ban])
    end

    def geocoder
      Geocoder.new(SETTINGS[:geocoder])
    end

    def apertium
      Apertium.new(SETTINGS[:apertium])
    end

    def mailer
      Mailer.new(SETTINGS[:mailer])
    end

    def queues
      Queues.new(SETTINGS[:queues])
    end
  end
end
