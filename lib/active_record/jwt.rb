# frozen_string_literal: true

module ActiveRecord
  # To include JWT methods
  module JWT
    extend ActiveSupport::Concern

    # JWT methods
    module ClassMethods
      def has_jwt(attribute = :key) # rubocop:disable Naming/PredicateName
        has_secure_token attribute
      end
    end

    included do
      def jwt_decode(token, attribute = :key)
        ::JWT.decode(token, self[attribute], true, verify_iat: true)[0]
      rescue ::JWT::DecodeError => e
        logger.error e.message
        logger.error e.backtrace.join("\n")
        nil
      end

      def jwt_encode(payload, attribute = :key)
        ::JWT.encode(payload, self[attribute])
      rescue ::JWT::EncodeError => e
        logger.error e.message
        logger.error e.backtrace.join("\n")
        nil
      end

      def self.generate_unique_secure_token
        SecureRandom.base58(32)
      end
    end
  end
end
