# frozen_string_literal: true

module Bululu
  # Authorization
  class Authorization
    # @return [ActionDispatch::Request]
    attr_reader :request

    # @param request [ActionDispatch::Request]
    def initialize(request:)
      @request = request

      set_locale
    end

    # To get the current client app
    #
    # @return [ClientApp, nil] Return the current client app or nil
    def current_client_app
      return @current_client_app if @current_client_app

      @current_client_app = client_app_from_headers || current_session&.client_app
    end

    # To get the current session
    #
    # @return [Session, nil] Return the current session or nil
    def current_session
      return @current_session if @current_session

      @current_session = session_from_headers
    end

    # To get the current user
    #
    # @return [User, nil] Return the current user or nil
    def current_user
      return nil unless current_session

      return @current_user if @current_user

      @current_user = current_session.user
    end

    def current_scopes
      current_session&.client_app&.scopes || current_client_app&.scopes
    end

    # @return [Boolean]
    def client_app_authorized?
      current_client_app.present?
    end

    # @return [Boolean]
    def user_authenticated?
      current_user.present?
    end

    private

    def set_locale
      locales = request.headers['Accept-Language']

      return if locales.blank?

      locales_array = locales.strip.split(/\s*,\s*/)

      selected_locales = locales_array.select { |l| Settings.locales.include?(l) }

      if !selected_locales.empty?
        I18n.locale = selected_locales[0]
      else
        selected_languages = locales_array.collect { |l| l.split(/-|_/)[0].downcase }
                                          .select { |l| Settings.locales.include?(l) }

        I18n.locale = selected_languages[0] unless selected_languages.empty?
      end
    end

    def session_from_headers
      session_id = request.headers['X-Session-Id']
      session_token = request.headers['X-Session-Token']

      return unless session_id.present? && session_token.present?

      session = Session.find(session_id)
      session_payload = session.jwt_decode(session_token)

      if session_payload['clientAppId'].to_i == session.client_app_id &&
         session_payload['userId'].to_i == session.user_id
        session
      end
    rescue StandardError => e
      Rails.logger.error e.message
      Rails.logger.error e.backtrace.join("\n")
      nil
    end

    def client_app_from_headers
      client_app_id = request.headers['X-Client-App-Id']
      client_app_token = request.headers['X-Client-App-Token']

      return unless client_app_id.present? && client_app_token.present?

      client_app = ClientApp.find(client_app_id)
      client_app_payload = client_app.jwt_decode(client_app_token)

      client_app if client_app_payload
    rescue StandardError => e
      Rails.logger.error e.message
      Rails.logger.error e.backtrace.join("\n")
      nil
    end
  end
end
