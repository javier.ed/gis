# frozen_string_literal: true

module Bululu
  # To manage Fail2ban logfile
  module Fail2ban
    extend ActiveSupport::Concern
    included do
      # Add a host to Fail2ban logfile
      #
      # @param description [String] Description
      # @param host [String] Remote host
      def add_to_fail2ban(host, description)
        return unless Settings.fail2ban.enable?

        Logger.new(Settings.fail2ban.logfile).info("#{description} | FROM #{host}")
      end
    end
  end
end
