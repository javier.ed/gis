# frozen_string_literal: true

require 'active_support/concern'

module Bululu
  module Graphql
    # Authenticate user
    module AuthenticateUser
      extend ActiveSupport::Concern

      included do
        def self.visible?(context)
          (super && context.user_authenticated?) || context.introspection?
        end

        def self.authorized?(object, context)
          super && context.user_authenticated?
        end
      end
    end

    # Require no authentication
    module RequireNoAuthentication
      extend ActiveSupport::Concern

      included do
        def self.visible?(context)
          (super && !context.user_authenticated?) || context.introspection?
        end

        def self.authorized?(object, context)
          super && !context.user_authenticated?
        end
      end
    end

    # Scopes
    module Scopes
      def self.scopes_valid?(context, scopes)
        (
          context.current_scopes&.count&.positive? &&
          (scopes.map { |s| "!#{s}" } & context.current_scopes).count.zero? && (
            context.current_scopes.include?('all') || (context.current_scopes & scopes).count == scopes.count
          )
        ) || context.introspection?
      end

      extend ActiveSupport::Concern
      included do
        def self.required_scopes(*scopes)
          @scopes = scopes
        end

        def self.visible?(context)
          super && Scopes.scopes_valid?(context, @scopes || [])
        end

        def self.authorized?(object, context)
          super && Scopes.scopes_valid?(context, @scopes || [])
        end
      end
    end
  end
end
