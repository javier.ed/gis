# frozen_string_literal: true

module Settings
  # Queues settings
  class Queues
    def initialize(settings)
      @settings = settings
    end

    def redis_url
      @settings[:redis_url]
    end
  end
end
