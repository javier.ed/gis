# frozen_string_literal: true

module Settings
  # Mailer settings
  class Mailer
    def initialize(settings)
      @settings = settings
    end

    def enable?
      return false if Rails.env.test?

      @settings[:enable]
    end

    def sender_address
      @settings[:sender_address]
    end

    def method
      @settings[:method]
    end

    def location
      return nil unless @settings[:method] == 'sendmail'

      @settings[:location]
    end

    def exim_fix
      return nil unless @settings[:method] == 'sendmail'

      @settings[:exim_fix]
    end

    def address
      return nil unless @settings[:method] == 'smtp'

      @settings[:address]
    end

    def port
      return nil unless @settings[:method] == 'smtp'

      @settings[:port]
    end

    def authentication
      return nil unless @settings[:method] == 'smtp'

      @settings[:authentication]
    end

    def username
      return nil unless @settings[:method] == 'smtp'

      @settings[:username]
    end

    def password
      return nil unless @settings[:method] == 'smtp'

      @settings[:password]
    end

    def enable_starttls_auto?
      return nil unless @settings[:method] == 'smtp'

      @settings[:enable_starttls_auto]
    end

    def domain
      return nil unless @settings[:method] == 'smtp'

      @settings[:domain]
    end

    def openssl_verify_mode
      return nil unless @settings[:method] == 'smtp'

      @settings[:openssl_verify_mode]
    end

    def ca_file
      return nil unless @settings[:method] == 'smtp'

      @settings[:ca_file]
    end
  end
end
