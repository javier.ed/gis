# frozen_string_literal: true

module Settings
  # Apertium settings
  class Apertium
    def initialize(settings)
      @settings = settings
    end

    def apy_url
      URI.parse(@settings[:apy_url])
    end

    def apy_translate_url
      apy_url.merge("#{apy_url.path}/translate")
    end
  end
end
