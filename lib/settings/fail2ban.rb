# frozen_string_literal: true

module Settings
  # Fail2ban settings
  class Fail2an
    def initialize(settings)
      @settings = settings
    end

    def enable?
      @settings[:enable]
    end

    def logfile
      @settings[:logfile]
    end
  end
end
