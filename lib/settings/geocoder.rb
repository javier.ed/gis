# frozen_string_literal: true

module Settings
  # Geocoder settings
  class Geocoder
    def initialize(settings)
      @settings = settings
    end

    def redis_url
      @settings[:redis_url]
    end

    def cache
      return nil if redis_url.blank?

      Redis.new(url: redis_url)
    end
  end
end
