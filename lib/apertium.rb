# frozen_string_literal: true

require 'net/http'
require 'settings'

# Apertium module
module Apertium
  # @param text [String]
  # @param source_language [String]
  # @param target_language [String]
  # @return [String]
  def self.translate(text, source_language, target_language)
    uri = Settings.apertium.apy_translate_url
    uri.query = URI.encode_www_form({ q: text, langpair: "#{source_language}|#{target_language}" })

    request = Net::HTTP::Get.new uri
    request.content_type = 'application/json'

    response = Net::HTTP.start(uri.host, uri.port, use_ssl: (uri.scheme == 'https')) do |http|
      http.request request
    end

    translated_text = JSON.parse(response.body)['responseData']['translatedText']

    yield translated_text if block_given?
    translated_text
  rescue StandardError => e
    Rails.logger.error e.message
    Rails.logger.error e.backtrace.join("\n")
    nil
  end
end
