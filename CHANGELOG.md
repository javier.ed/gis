# CHANGELOG


## [0.2.0] - 2020-09-23

### Added

* Added feature to translate content after save !37 !35 !31
* Added GraphQL queries to locate favorite markers !36

### Changed

* Dependencies upgraded !32

### Removed

* Removed data argument from marker mutations !34
* MarkerData removed !33


## [0.1.0] - 2020-07-28

### Added

* Added layer settings to set time intervals and to allow or disallow suggestions !28
* Added time interval fields to GraphQL type marker !27
* Added feature to clone markers !26
* Added feature to change email !21
* Added feature to update profile !20

### Changed

* Upgrade dependencies !22

### Fixed

* Fixed error response at GraphQL mutation to change email !30

### Removed

* Removed datetime param from method Marker.search !29
* Removed MarkerData !25


## [0.0.0] - 2020-07-15

### Added

* All is new
* Added feature to change password !19
* Added feature to subscribe or unsubscribe from layers !18
* Added features to add or remove favorites !17
* Added more client app scopes !16
* Added feature to delete layers !15
* Added feature to delete markers !14
* Added feature to approve or reject markers !13
* Added feature to update markers !12
* Added a required scope for client apps to create users !11
* License added !10
* Added GitLab CI !8
* Added feature to update layers !7
* Added feature to create markers !6
* Added feature to show layers !5
* Added feature to create layers !4
* Added feature to reset passwords !3
* Added feature to login and logout users !2
* Added feature to register users !1

### Removed

* `NominatimData` model removed !9
