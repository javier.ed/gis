# frozen_string_literal: true

Globalize.fallbacks = { en: %i[en es], es: %i[es en] }
