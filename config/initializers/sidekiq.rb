# frozen_string_literal: true

if Rails.env.production?
  Rails.application.configure do
    config.active_job.queue_adapter = :sidekiq
  end
end
