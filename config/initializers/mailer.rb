# frozen_string_literal: true

require 'settings'

Rails.application.configure do
  config.action_mailer.perform_deliveries = Settings.mailer.enable?

  config.action_mailer.default_url_options = {
    host: Settings.host,
    port: Settings.port,
    protocol: Settings.protocol
  }

  unless Rails.env.test? || !Settings.mailer.enable?
    if Settings.mailer.method == 'sendmail'
      config.action_mailer.delivery_method = :sendmail
      sendmail_settings = { location: Settings.mailer.location }
      sendmail_settings[:arguments] = '-i' if Settings.mailer.exim_fix
      config.action_mailer.sendmail_settings = sendmail_settings
    elsif Settings.mailer.method == 'smtp'
      config.action_mailer.delivery_method = :smtp
      smtp_settings = {
        address: Settings.mailer.address,
        port: Settings.mailer.port.to_i,
        domain: Settings.mailer.domain,
        openssl_verify_mode: Settings.mailer.openssl_verify_mode,
        ca_file: Settings.mailer.ca_file,
        enable_starttls_auto: false
      }

      if Settings.mailer.authentication != 'none'
        smtp_settings.merge!(
          authentication: Settings.mailer.authentication.tr('-', '_').to_sym,
          user_name: Settings.mailer.username,
          password: Settings.mailer.password,
          enable_starttls_auto: Settings.mailer.enable_starttls_auto?
        )
      end

      config.action_mailer.smtp_settings = smtp_settings
    else
      warn "WARNING: Mailer turned on with unknown method #{Settings.mailer.method}. Mail won't work."
    end
  end
end
