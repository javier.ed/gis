# frozen_string_literal: true

require 'settings'

Rails.application.configure do
  unless Rails.env.test?
    config.hosts << Settings.host
    config.hosts << 'localhost'
  end
end
