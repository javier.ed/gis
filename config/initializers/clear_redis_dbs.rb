# frozen_string_literal: true

require 'settings'

Redis.new(url: Settings.geocoder.redis_url).flushdb if Settings.geocoder.redis_url.present?

Redis.new(url: Settings.queues.redis_url).flushdb if Settings.queues.redis_url.present?
