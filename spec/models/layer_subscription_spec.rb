# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LayerSubscription, type: :model do
  describe 'fields' do
    it { is_expected.to have_db_column(:user_id).of_type(:integer) }
    it { is_expected.to have_db_column(:layer_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_index(:user_id) }
    it { is_expected.to have_db_index(:layer_id) }
    it { is_expected.to have_db_index(%i[user_id layer_id]).unique }
  end

  describe 'associations' do
    it { is_expected.to belong_to(:user).optional }
    it { is_expected.to belong_to(:layer) }
  end

  describe 'validations' do
    subject { build(:layer_subscription) }

    it { is_expected.to validate_uniqueness_of(:layer_id).scoped_to(:user_id) }
  end
end
