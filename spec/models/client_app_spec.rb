# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ClientApp, type: :model do
  describe 'fields' do
    it { is_expected.to have_db_column(:key).of_type(:string).with_options(null: false) }
    it { is_expected.to have_db_column(:description).of_type(:string).with_options(null: false, limit: 256) }
    it { is_expected.to have_db_column(:scopes).of_type(:string).with_options(null: false, limit: 256) }
    it { is_expected.to have_db_index(:key).unique }
  end

  describe 'associations' do
    it { is_expected.to have_many(:sessions).dependent(:restrict_with_exception) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of :description }
    it { is_expected.to validate_presence_of :scopes }
    it { is_expected.to validate_length_of(:description).is_at_most(256) }
  end

  describe '#scopes' do
    let(:client_app) { build :client_app }

    context 'when it is getting the value' do
      it { expect(client_app.scopes).to eq ['all'] }
    end

    context 'when it is assigning the value' do
      it do
        client_app.scopes = 'some_scope'
        expect(client_app.scopes).to eq ['some_scope']
      end
    end

    context 'when it is created' do
      it do
        client_app.save
        expect(described_class.all.last.scopes).to eq ['all']
      end
    end
  end
end
