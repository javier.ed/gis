# frozen_string_literal: true

require 'rails_helper'
require 'settings'

RSpec.describe User, type: :model do
  describe 'fields' do
    it { is_expected.to have_db_column(:username).of_type(:string).with_options(null: false, limit: 24) }
    it { is_expected.to have_db_column(:password_digest).of_type(:string).with_options(null: false) }
    it { is_expected.to have_db_column(:email).of_type(:string).with_options(null: false, limit: 256) }
    it { is_expected.to have_db_column(:email_verified_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:created_at).of_type(:datetime).with_options(null: false) }
    it { is_expected.to have_db_column(:updated_at).of_type(:datetime).with_options(null: false) }
    it { is_expected.to have_db_index(:username).unique }
    it { is_expected.to have_db_index(:email).unique }
    it { is_expected.to have_secure_password }
  end

  describe 'associations' do
    it { is_expected.to have_many(:sessions).dependent(:restrict_with_exception) }
    it { is_expected.to have_many(:otps).dependent(:restrict_with_exception) }
    it { is_expected.to have_many(:layers).dependent(:restrict_with_exception) }
    it { is_expected.to have_many(:markers).dependent(:restrict_with_exception) }
    it { is_expected.to have_many(:marker_approvals).dependent(:restrict_with_exception) }
    it { is_expected.to have_many(:favorite_markers).dependent(:restrict_with_exception) }
    it { is_expected.to have_many(:layer_subscriptions).dependent(:restrict_with_exception) }
    it { is_expected.to have_one(:profile).dependent(:restrict_with_exception) }
  end

  describe 'validations' do
    subject { build(:user) }

    it { is_expected.to validate_presence_of(:username) }
    it { is_expected.to validate_length_of(:username).is_at_least(5).is_at_most(24) }
    it { is_expected.to allow_value('valid_username').for(:username) }
    it { is_expected.not_to allow_value('invalid username').for(:username) }
    it { is_expected.to validate_exclusion_of(:username).in_array(Settings.username_blacklist) }
    it { is_expected.to validate_uniqueness_of(:username).case_insensitive }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to validate_length_of(:email).is_at_least(5).is_at_most(256) }
    it { is_expected.to allow_value('valid@email.tld').for(:email) }
    it { is_expected.not_to allow_value('invalid email').for(:email) }
    it { is_expected.to validate_uniqueness_of(:email).case_insensitive }
    it { is_expected.to validate_confirmation_of(:password) }
    it { is_expected.to validate_timeliness_of :email_verified_at }
  end

  describe '.create' do
    let(:user) { create :user }

    before do
      create :layer_subscription, user: nil
    end

    it { expect(user.otps.count).to eq 1 }
    it { expect(user.layer_subscriptions.count).to eq 1 }
  end

  describe '#verify_email' do
    let(:user) { create :user }
    let(:otp) { create :otp, user: user, action: 'verify_email' }

    context 'when all data is valid' do
      it { expect(user.verify_email(otp.password)).to eq true }
    end

    context 'when the email is already verified' do
      it do
        user.verify_email(otp.password)
        expect(user.verify_email(otp.password)).to eq false
      end
    end
  end

  describe '#change_email' do
    let(:user) { create :user }
    let(:email) { 'new@email.address' }

    context 'when the email is valid' do
      it { expect(user.change_email(email)).to eq true }
    end

    context 'when the email is invalid' do
      it { expect(user.change_email('wrong email')).to eq false }
    end

    context 'when the email is the same in uppercase' do
      it { expect(user.change_email(user.email.upcase)).to eq false }
    end
  end

  describe '#resend_email_verification' do
    let(:user) { create :user }

    context 'when the email isn\'t verified' do
      it { expect(user.resend_email_verification).to eq true }
    end

    context 'when the email is already verified' do
      it do
        user.update email_verified_at: Time.current
        expect(user.resend_email_verification).to eq false
      end
    end
  end

  describe '#change_password' do
    let(:user) { create :user }

    context 'when the password is valid' do
      it { expect(user.change_password('valid password', 'valid password')).to eq true }
    end

    context 'when there is no confirmation' do
      it { expect(user.change_password('valid password')).to eq true }
    end

    context 'when the password is too short' do
      it { expect(user.change_password('short', 'short')).to eq false }
    end

    context 'when the password does not match with the confirmation' do
      it { expect(user.change_password('valid password', 'invalid password')).to eq false }
    end

    context 'when the password is blank' do
      it { expect(user.change_password('', '')).to eq false }
    end
  end

  describe '.find_by_login' do
    let(:user) { create :user_with_verified_email }

    context 'when login is a valid username ' do
      it { expect(described_class.find_by_login(user.username)).to be_truthy }
    end

    context 'when login is an uppercase username' do
      it { expect(described_class.find_by_login(user.username.upcase)).to be_truthy }
    end

    context 'when login is an invalid username' do
      it { expect(described_class.find_by_login('wrong username')).to be_nil }
    end

    context 'when login is a verified email' do
      it { expect(described_class.find_by_login(user.email)).to be_truthy }
    end

    context 'when login is an uppercase email' do
      it { expect(described_class.find_by_login(user.email.upcase)).to be_truthy }
    end

    context 'when login is an unverified email' do
      let(:user) { create :user }

      it { expect(described_class.find_by_login(user.email)).to be_nil }
    end

    context 'when login is an invalid email' do
      it { expect(described_class.find_by_login('wrong email')).to be_nil }
    end
  end

  describe '.find_by_username' do
    let(:user) { create :user }

    context 'when the username is valid ' do
      it { expect(described_class.find_by_username(user.username)).to be_truthy }
    end

    context 'when the username is invalid' do
      it { expect(described_class.find_by_username('wrong username')).to be_nil }
    end

    context 'when the username is uppercase' do
      it { expect(described_class.find_by_login(user.username.upcase)).to be_truthy }
    end
  end
end
