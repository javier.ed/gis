# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Session, type: :model do
  describe 'fields' do
    it { is_expected.to have_db_column(:key).of_type(:string).with_options(null: false) }
    it { is_expected.to have_db_column(:finished_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:client_app_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:user_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_index(:key).unique }
    it { is_expected.to have_db_index(:client_app_id) }
    it { is_expected.to have_db_index(:user_id) }
  end

  describe 'associations' do
    it { is_expected.to belong_to :client_app }
    it { is_expected.to belong_to :user }
  end

  describe 'validations' do
    it { is_expected.to validate_timeliness_of :finished_at }
  end
end
