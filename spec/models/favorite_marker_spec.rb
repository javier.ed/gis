# frozen_string_literal: true

require 'rails_helper'

RSpec.describe FavoriteMarker, type: :model do
  describe 'fields' do
    it { is_expected.to have_db_column(:user_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:marker_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_index(:user_id) }
    it { is_expected.to have_db_index(:marker_id) }
    it { is_expected.to have_db_index(%i[user_id marker_id]).unique }
  end

  describe 'associations' do
    it { is_expected.to belong_to(:user) }
    it { is_expected.to belong_to(:marker) }
  end

  describe 'validations' do
    subject { build(:favorite_marker) }

    it { is_expected.to validate_uniqueness_of(:marker_id).scoped_to(:user_id) }
  end

  describe '#locate' do
    let(:favorite_marker) { create(:favorite_marker) }

    context 'when the coordinates are valid' do
      it do
        expect(
          described_class.locate(
            favorite_marker.marker.geocoding_point.latitude, favorite_marker.marker.geocoding_point.longitude
          ).count(:all)
        ).to(eq(1))
      end
    end
  end
end
