# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Layer, type: :model do
  describe 'fields' do
    it do
      expect(described_class.new).to(
        have_db_column(:time_interval).of_type(:string).with_options(null: false, default: 'any')
      )
    end

    it { is_expected.to have_db_column(:start_at_current_time).of_type(:boolean) }

    it do
      expect(described_class.new).to(
        have_db_column(:suggestions).of_type(:string).with_options(null: false, default: 'requires_approval')
      )
    end

    it { is_expected.to have_db_column(:user_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_index(:user_id) }
    it { is_expected.to accept_nested_attributes_for(:fields) }
  end

  describe 'associations' do
    it do
      expect(described_class.new).to(
        have_many(:fields).class_name('LayerField').dependent(:destroy).autosave(true)
      )
    end

    it { is_expected.to have_many(:markers).dependent(:destroy) }
    it { is_expected.to have_many(:subscriptions).class_name('LayerSubscription').dependent(:destroy) }
  end

  describe 'validations' do
    subject { build(:layer) }

    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:description) }
    it { is_expected.to validate_presence_of(:time_interval) }
    it { is_expected.to validate_length_of(:name).is_at_most(256) }
    it { is_expected.to validate_inclusion_of(:suggestions).in_array(%w[requires_approval publish_directly deny]) }
    it { is_expected.to allow_value('any').for(:time_interval) }
    it { is_expected.to allow_value('disabled').for(:time_interval) }
    it { is_expected.to allow_value('1 day').for(:time_interval) }
    it { is_expected.not_to allow_value('wrong value').for(:time_interval) }
  end

  describe '.search' do
    before { create(:layer, name: 'Some layer') }

    context 'when the query has one result' do
      it { expect(described_class.search('Some layer', order: { name: :desc }).count(:all).size).to eq 1 }
    end

    context 'when the query has zero results' do
      it { expect(described_class.search('no layer', order: { name: :desc }).count(:all).size).to eq 0 }
    end

    context 'when the order params are invalid' do
      it { expect(described_class.search('Some layer', order: { bad: :params })).to be_falsey }
    end
  end

  describe '#pending_markers' do
    let(:marker) { create :marker }
    let(:layer) { marker.layer }

    context 'when there is a pending marker' do
      it { expect(layer.pending_markers.count).to eq 1 }
    end

    context 'when there is a no pending markers' do
      let(:marker) { create :marker, published_at: Time.current }

      it { expect(layer.pending_markers.count).to eq 0 }
    end
  end
end
