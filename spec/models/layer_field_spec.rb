# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LayerField, type: :model do
  describe 'fields' do
    it { is_expected.to have_db_column(:field_type).of_type(:string).with_options(null: false) }
    it { is_expected.to have_db_column(:required).of_type(:boolean) }
    it { is_expected.to have_db_column(:unique).of_type(:boolean) }
    it { is_expected.to have_db_column(:editable).of_type(:boolean) }
    it { is_expected.to have_db_column(:layer_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_index(:layer_id) }
    it { is_expected.to accept_nested_attributes_for(:options) }
    it { is_expected.to have_readonly_attribute(:layer_id) }
  end

  describe 'associations' do
    it { is_expected.to belong_to(:layer) }
    it { is_expected.to have_many(:options).class_name('LayerFieldOption').dependent(:destroy) }

    it { is_expected.to have_many(:marker_values).dependent(:destroy) }
  end

  describe 'validations' do
    subject { build(:layer).fields.first }

    it { is_expected.to validate_presence_of(:field_type) }
    it { is_expected.to validate_presence_of(:name) }

    it do
      expect(described_class.new).to(
        validate_inclusion_of(:field_type).in_array(%w[string text integer float boolean date time datetime choice])
      )
    end

    it { is_expected.to validate_length_of(:name).is_at_most(256) }
    it { is_expected.to validate_uniqueness_of(:name).case_insensitive.scoped_to(:layer_id) }
  end
end
