# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LayerFieldOption, type: :model do
  describe 'fields' do
    it { is_expected.to have_db_column(:layer_field_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_index(:layer_field_id) }
  end

  describe 'associations' do
    it { is_expected.to belong_to(:layer_field) }
  end

  describe 'validations' do
    subject { build(:layer).fields.last.options.first }

    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_length_of(:name).is_at_most(256) }
    it { is_expected.to validate_uniqueness_of(:name).case_insensitive.scoped_to(:layer_field_id) }
  end
end
