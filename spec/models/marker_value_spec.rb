# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MarkerValue, type: :model do
  describe 'fields' do
    it { is_expected.to have_db_column(:integer_value).of_type(:integer) }
    it { is_expected.to have_db_column(:float_value).of_type(:float) }
    it { is_expected.to have_db_column(:boolean_value).of_type(:boolean) }
    it { is_expected.to have_db_column(:datetime_value).of_type(:datetime) }
    it { is_expected.to have_db_column(:date_value).of_type(:date) }
    it { is_expected.to have_db_column(:time_value).of_type(:time) }
    it { is_expected.to have_db_column(:layer_field_option_id).of_type(:integer) }
    it { is_expected.to have_db_column(:layer_field_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:marker_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_index(:layer_field_option_id) }
    it { is_expected.to have_db_index(:marker_id) }
    it { is_expected.to have_db_index(:layer_field_id) }

    it { is_expected.to have_db_index(%i[marker_id layer_field_id]).unique }
  end

  describe 'associations' do
    it { is_expected.to belong_to(:layer_field_option).optional }
    it { is_expected.to belong_to(:layer_field).inverse_of(:marker_values) }
    it { is_expected.to belong_to(:marker).inverse_of(:values) }
  end

  describe 'validations' do
    subject { build(:marker).values.first }

    it { is_expected.to validate_uniqueness_of(:marker_id).scoped_to(:layer_field_id) }
  end
end
