# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Marker, type: :model do
  describe 'fields' do
    it { is_expected.to have_db_column(:layer_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:geocoding_point_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:user_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:published_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:start_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:finish_at).of_type(:datetime) }
    it { is_expected.to have_db_index(:layer_id) }
    it { is_expected.to have_db_index(:geocoding_point_id) }
    it { is_expected.to have_db_index(:user_id) }
    it { is_expected.to accept_nested_attributes_for(:values) }
  end

  describe 'associations' do
    it { is_expected.to belong_to :layer }
    it { is_expected.to belong_to :geocoding_point }
    it { is_expected.to belong_to :user }
    it { is_expected.to have_many(:values).class_name('MarkerValue').dependent(:destroy).autosave(true) }
    it { is_expected.to have_many(:approvals).class_name('MarkerApproval').dependent(:destroy) }
    it { is_expected.to belong_to(:marker_approval).optional }
    it { is_expected.to have_many(:favorite_markers).dependent(:destroy) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_length_of(:name).is_at_most(256) }
    it { is_expected.to validate_content_type_of(:avatar).allowing('image/gif', 'image/jpeg', 'image/png') }
    it { is_expected.to validate_content_type_of(:avatar).rejecting('text/plain', 'text/xml') }
    it { is_expected.to validate_timeliness_of :start_at }
    it { is_expected.to validate_timeliness_of :finish_at }
  end

  describe '#search' do
    let!(:marker) { create(:marker, published_at: Time.current) }

    context 'when the query is the name of the marker' do
      it do
        expect(described_class.search(marker.name, layer_id: marker.layer_id).count(:all).size).to eq 1
      end
    end

    context 'when the query is the address' do
      it { expect(described_class.search(marker.geocoding_point.address).count(:all).size).to eq 1 }
    end
  end

  describe '#locate' do
    let(:marker) { create(:marker, published_at: Time.current) }

    context 'when the coordinates are valid' do
      it do
        expect(
          described_class.locate(
            marker.geocoding_point.latitude, marker.geocoding_point.longitude, layer_id: marker.layer_id
          ).count(:all)
        ).to(eq(1))
      end
    end
  end

  describe '.unpublish' do
    context 'when the marker is published' do
      let(:marker) { create(:marker, published_at: Time.current) }

      it { expect(marker.unpublish).to eq true }
    end

    context 'when the marker is already unpublished' do
      let(:marker) { create(:marker) }

      it { expect(marker.unpublish).to eq false }
    end
  end

  describe '.build_duplicate' do
    context 'when arguments are valid' do
      let(:marker) { create(:marker, published_at: Time.current) }
      let(:user) { create(:user) }

      it { expect(marker.build_duplicate(user: user)).to be_a described_class }
      it { expect(marker.build_duplicate(user: user).user).to eq user }
      it { expect(marker.build_duplicate(user: user).published_at.present?).to eq false }
      it { expect(marker.build_duplicate(user: user).avatar.attached?).to eq true }
    end
  end

  describe 'validate geocoding point' do
    let(:marker) { create :marker, published_at: Time.current }
    let(:geocoding_point) { marker.geocoding_point }
    let(:clone_marker) { marker.build_duplicate(published_at: Time.current) }

    context 'when the geocoding point is valid' do
      let(:clone_marker) do
        marker.build_duplicate(
          published_at: Time.current,
          geocoding_point: build(
            :geocoding_point, latitude: geocoding_point.latitude + 0.0001, longitude: geocoding_point.longitude - 0.0001
          )
        )
      end

      it { expect(clone_marker.save).to eq true }
    end

    context 'when the geocoding point is the same' do
      it { expect(clone_marker.save).to eq false }
    end

    context 'when the geocoding point is near other marker' do
      let(:clone_marker) do
        marker.build_duplicate(
          published_at: Time.current,
          geocoding_point: build(
            :geocoding_point, latitude: geocoding_point.latitude + 0.00001,
                              longitude: geocoding_point.longitude - 0.00001
          )
        )
      end

      it { expect(clone_marker.save).to eq false }
    end

    context 'when the geocoding point is the same but start after the other finish' do
      let(:marker) { create :marker, published_at: Time.current, finish_at: Time.current }
      let(:clone_marker) do
        marker.build_duplicate(published_at: Time.current, start_at: marker.finish_at, finish_at: nil)
      end

      it { expect(clone_marker.save).to eq true }
    end

    context 'when the geocoding point is the same but finish before the other start' do
      let(:marker) { create :marker, published_at: Time.current, start_at: Time.current }
      let(:clone_marker) do
        marker.build_duplicate(published_at: Time.current, start_at: nil, finish_at: marker.start_at)
      end

      it { expect(clone_marker.save).to eq true }
    end

    context 'when the geocoding point is the same and start before the other finish' do
      let(:marker) { create :marker, published_at: Time.current, finish_at: Time.current }
      let(:clone_marker) do
        marker.build_duplicate(published_at: Time.current, start_at: marker.finish_at - 1.second, finish_at: nil)
      end

      it { expect(clone_marker.save).to eq false }
    end

    context 'when the geocoding point is the same and finish after the other start' do
      let(:marker) { create :marker, published_at: Time.current, start_at: Time.current }
      let(:clone_marker) do
        marker.build_duplicate(published_at: Time.current, start_at: nil, finish_at: marker.start_at + 1.second)
      end

      it { expect(clone_marker.save).to eq false }
    end
  end

  describe 'validate time interval' do
    let(:current_time) { Time.current }
    let(:marker) { build :marker, layer: layer, start_at: current_time, finish_at: current_time + 1.day }

    context 'when the time interval could be any' do
      let(:layer) { create :layer, time_interval: 'any' }

      it { expect(marker.save).to eq true }

      it do
        marker.finish_at = marker.start_at - 1.second
        expect(marker.save).to eq false
      end
    end

    context 'when de time interval is 1 day' do
      let(:layer) { create :layer, time_interval: '1 day' }

      it { expect(marker.save).to eq true }

      it do
        marker.finish_at = marker.start_at + 2.days
        expect(marker.save).to eq false
      end
    end

    context 'when de time interval is disabled' do
      let(:layer) { create :layer, time_interval: 'disabled' }
      let(:marker) { build :marker, layer: layer }

      it { expect(marker.save).to eq true }

      it do
        marker.start_at = current_time
        expect(marker.save).to eq false
      end
    end

    context 'when de time interval must start at current time' do
      let(:layer) { create :layer, start_at_current_time: true }

      it { expect(marker.save).to eq true }

      it do
        marker.start_at = nil
        expect(marker.save).to eq false
      end
    end
  end
end
