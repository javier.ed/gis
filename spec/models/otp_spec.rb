# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Otp, type: :model do
  describe 'fields' do
    it { is_expected.to have_db_column(:action).of_type(:string).with_options(null: false) }
    it { is_expected.to have_db_column(:password_digest).of_type(:string).with_options(null: false) }
    it { is_expected.to have_db_column(:used_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:discarded_at).of_type(:datetime) }
    it { is_expected.to have_db_column(:attempts).of_type(:integer).with_options(default: 0, null: false) }
    it { is_expected.to have_db_column(:user_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_index(:user_id) }
    it { is_expected.to have_secure_password }
  end

  describe 'associations' do
    it { is_expected.to belong_to :user }
  end

  describe 'validations' do
    it { is_expected.to validate_inclusion_of(:action).in_array(%w[verify_email reset_password]) }

    it do
      expect(described_class.new).to(
        validate_numericality_of(:attempts).only_integer.is_greater_than_or_equal_to(0).is_less_than_or_equal_to(3)
      )
    end

    it { is_expected.to validate_timeliness_of :used_at }
    it { is_expected.to validate_timeliness_of :discarded_at }
  end

  describe '#find_and_authenticate' do
    let(:otp) { create :otp }

    context 'when params are valid' do
      it do
        expect(otp.user.otps.find_and_authenticate(otp.action, otp.password)).to be_truthy
      end
    end

    context 'when the password is invalid' do
      it do
        expect(otp.user.otps.find_and_authenticate(otp.action, 'wrong password')).to be_falsey
      end
    end

    context 'when there are too many attempts' do
      it do
        otp.update(attempts: 3)
        expect(otp.user.otps.find_and_authenticate(otp.action, otp.password)).to be_falsey
      end
    end
  end

  describe '.mark_as_used' do
    let(:otp) { create :otp }

    it { expect(otp.mark_as_used).to eq true }
  end
end
