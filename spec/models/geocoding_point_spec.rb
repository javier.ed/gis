# frozen_string_literal: true

require 'rails_helper'

RSpec.describe GeocodingPoint, type: :model do
  describe 'fields' do
    it { is_expected.to have_db_column(:latitude).of_type(:float).with_options(null: false) }
    it { is_expected.to have_db_column(:longitude).of_type(:float).with_options(null: false) }
  end

  describe 'associations' do
    it { is_expected.to have_many(:markers).dependent(:restrict_with_exception) }
  end

  describe 'validations' do
    subject { build(:geocoding_point) }

    it { is_expected.to validate_presence_of(:latitude) }
    it { is_expected.to validate_presence_of(:longitude) }

    it do
      expect(described_class.new).to(
        validate_numericality_of(:latitude).is_greater_than_or_equal_to(-90).is_less_than_or_equal_to(90)
      )
    end

    it do
      expect(described_class.new).to(
        validate_numericality_of(:longitude).is_greater_than_or_equal_to(-180).is_less_than_or_equal_to(180)
      )
    end

    it { is_expected.to validate_uniqueness_of(:latitude).scoped_to(:longitude) }
    it { is_expected.to validate_length_of(:address).is_at_most(256) }
  end

  describe '.search' do
    let!(:geocoding_point) { create :geocoding_point }

    context 'when the query is valid' do
      it { expect(described_class.search(geocoding_point.address.split(', ').first).count(:all)).to eq 1 }
    end
  end

  describe '.locate' do
    let!(:geocoding_point) { create :geocoding_point }

    context 'when the coordinates are valid' do
      it { expect(described_class.locate(geocoding_point.latitude, geocoding_point.longitude).count(:all)).to eq 1 }
    end
  end
end
