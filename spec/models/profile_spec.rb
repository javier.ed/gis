# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Profile, type: :model do
  describe 'fields' do
    it { is_expected.to have_db_column(:user_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:name).of_type(:string).with_options(limit: 256) }
    it { is_expected.to have_db_column(:bio).of_type(:text) }
    it { is_expected.to have_db_column(:country_id).of_type(:string) }
    it { is_expected.to have_db_column(:birthdate).of_type(:date) }
    it { is_expected.to have_db_index(:user_id).unique }
    it { is_expected.to have_readonly_attribute(:user_id) }
  end

  describe 'associations' do
    it { is_expected.to belong_to :user }
  end

  describe 'validations' do
    subject { build :profile }

    it { is_expected.to validate_uniqueness_of :user_id }
    it { is_expected.to validate_length_of(:name).is_at_most(256) }
    it { is_expected.to validate_inclusion_of(:country_id).in_array(ISO3166::Country.all.map(&:alpha2)).allow_blank }
    it { is_expected.to validate_timeliness_of :birthdate }
    it { is_expected.to validate_content_type_of(:avatar).allowing('image/gif', 'image/jpeg', 'image/png') }
    it { is_expected.to validate_content_type_of(:avatar).rejecting('text/plain', 'text/xml') }
  end

  describe '#country' do
    let(:profile) { create :profile }

    context 'when the country code is present' do
      it do
        expect(profile.country).to be_truthy
      end
    end

    context 'when the country code is empty' do
      let(:profile) { create :profile, country_id: '' }

      it do
        expect(profile.country).to be_nil
      end
    end
  end
end
