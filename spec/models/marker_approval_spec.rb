# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MarkerApproval, type: :model do
  describe 'fields' do
    it { is_expected.to have_db_column(:user_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:marker_id).of_type(:integer).with_options(null: false) }
    it { is_expected.to have_db_column(:approved).of_type(:boolean) }
    it { is_expected.to have_db_column(:comment).of_type(:text) }
    it { is_expected.to have_db_index(:marker_id) }
    it { is_expected.to have_db_index(:user_id) }
  end

  describe 'associations' do
    it { is_expected.to belong_to :user }
    it { is_expected.to belong_to :marker }
  end
end
