# frozen_string_literal: true

FactoryBot.define do
  factory :layer do
    sequence(:name) { |n| "Some layer #{n}" }
    sequence(:description) { |n| "Some description #{n}" }
    user { create :user }

    after :build do |layer|
      layer.fields.build(field_type: 'string', name: 'Some string field', required: true, editable: true)
      layer.fields.build(
        field_type: 'choice', name: 'Some choice field', options_attributes: [{ name: 'Some option' }], editable: true
      )
    end
  end
end
