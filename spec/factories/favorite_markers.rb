# frozen_string_literal: true

FactoryBot.define do
  factory :favorite_marker do
    user { create :user }
    marker { create :marker, published_at: Time.current }
  end
end
