# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:username) { |n| "foo_#{n}" }
    sequence(:email) { |n| "foo#{n}@bar.baz" }
    password { '12345678' }
    password_confirmation { '12345678' }

    factory :user_with_verified_email do
      after :create do |user|
        user.update email_verified_at: Time.current
      end
    end
  end
end
