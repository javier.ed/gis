# frozen_string_literal: true

FactoryBot.define do
  factory :client_app do
    sequence(:description) { |n| "Some client app #{n}" }
    scopes { ['all'] }
  end
end
