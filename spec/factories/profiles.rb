# frozen_string_literal: true

FactoryBot.define do
  factory :profile do
    user { create :user }
    name { 'Some name' }
    bio { 'Some text' }
    birthdate { Date.current }
    country_id { 'VE' }
  end
end
