# frozen_string_literal: true

FactoryBot.define do
  factory :layer_subscription do
    user { create :user }
    layer { create :layer }
  end
end
