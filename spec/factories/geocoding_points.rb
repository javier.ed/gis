# frozen_string_literal: true

FactoryBot.define do
  factory :geocoding_point do
    latitude { 10.4494466 }
    longitude { -64.1630154 }
    address { 'Cumaná' }
  end
end
