# frozen_string_literal: true

FactoryBot.define do
  factory :otp do
    action { 'reset_password' }
    user { create :user }
  end
end
