# frozen_string_literal: true

FactoryBot.define do
  factory :marker_data do
    marker { create :marker }

    after :build do |marker_data|
      marker_data.user = marker_data.marker.user
      marker_data.marker.layer.fields.each do |field|
        field_types_map = {
          'string' => { string_value: 'Some value' },
          'choice' => { layer_field_option_id: field.options&.first&.id }
        }

        marker_data.values.build layer_field: field, **field_types_map[field.field_type]
      end
    end
  end
end
