# frozen_string_literal: true

FactoryBot.define do
  factory :session do
    client_app { create :client_app }
    user { create :user }
  end
end
