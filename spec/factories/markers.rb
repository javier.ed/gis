# frozen_string_literal: true

FactoryBot.define do
  factory :marker do
    sequence(:name) { |n| "Some marker #{n}" }
    description { 'Some description' }
    avatar { Rack::Test::UploadedFile.new('spec/fixtures/image-256x256.png') }
    layer { create :layer }
    geocoding_point { create :geocoding_point }
    user { create :user }

    after :build do |marker|
      marker.layer.fields.each do |field|
        field_types_map = {
          'string' => { string_value: 'Some value' },
          'choice' => { layer_field_option_id: field.options&.first&.id }
        }

        marker.values.build layer_field: field, **field_types_map[field.field_type]
      end
    end
  end
end
