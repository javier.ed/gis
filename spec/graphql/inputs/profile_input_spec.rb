# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Inputs::ProfileInput, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:name, :bio, :country_id).of_type(GraphQL::Types::String) }
    it { expect(described_class).to have_graphql_argument(:birthdate).of_type(GraphQL::Types::ISO8601Date) }
    it { expect(described_class).to have_graphql_argument(:avatar).of_type(ApolloUploadServer::Upload) }
    it { expect(described_class).to have_graphql_argument(:remove_avatar).of_type(GraphQL::Types::Boolean) }
  end
end
