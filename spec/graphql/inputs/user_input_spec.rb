# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Inputs::UserInput, type: :graphql do
  describe 'arguments' do
    it do
      expect(described_class).to(
        have_graphql_argument(:username, :email, :password).of_type(GraphQL::Types::String).required
      )
    end

    it { expect(described_class).to have_graphql_argument(:password_confirmation).of_type(GraphQL::Types::String) }
  end
end
