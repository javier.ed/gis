# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Inputs::MarkerInput, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:layer_id).of_type(GraphQL::Types::ID) }

    it do
      expect(described_class).to have_graphql_argument(:geocoding_point).of_type(Inputs::GeocodingPointInput).required
    end

    it { expect(described_class).to have_graphql_argument(:name).of_type(GraphQL::Types::String).required }
    it { expect(described_class).to have_graphql_argument(:description).of_type(GraphQL::Types::String) }

    it do
      expect(described_class).to have_graphql_argument(:start_at, :finish_at).of_type(GraphQL::Types::ISO8601DateTime)
    end

    it { expect(described_class).to have_graphql_argument(:avatar).of_type(ApolloUploadServer::Upload) }

    it do
      expect(described_class).to have_graphql_arguments(:remove_avatar, :translate).of_type(GraphQL::Types::Boolean)
    end

    it { expect(described_class).to have_graphql_argument(:values).of_type([Inputs::Marker::ValueInput]) }
    it { expect(described_class).to have_graphql_argument(:locale).of_type(GraphQL::Types::String) }
  end
end
