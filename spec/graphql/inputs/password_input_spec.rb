# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Inputs::PasswordInput, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:password).of_type(GraphQL::Types::String).required }
    it { expect(described_class).to have_graphql_argument(:password_confirmation).of_type(GraphQL::Types::String) }
  end
end
