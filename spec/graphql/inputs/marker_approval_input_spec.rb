# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Inputs::MarkerApprovalInput, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:marker_id).of_type(GraphQL::Types::ID).required }
    it { expect(described_class).to have_graphql_argument(:approved).of_type(GraphQL::Types::Boolean) }
    it { expect(described_class).to have_graphql_argument(:comment).of_type(GraphQL::Types::String) }
  end
end
