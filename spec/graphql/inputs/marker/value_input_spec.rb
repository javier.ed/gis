# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Inputs::Marker::ValueInput, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:layer_field_id).of_type(GraphQL::Types::ID).required }

    it do
      expect(described_class).to have_graphql_argument(:string_value, :text_value).of_type(GraphQL::Types::String)
    end

    it { expect(described_class).to have_graphql_argument(:integer_value).of_type(GraphQL::Types::Int) }
    it { expect(described_class).to have_graphql_argument(:float_value).of_type(GraphQL::Types::Float) }
    it { expect(described_class).to have_graphql_argument(:boolean_value).of_type(GraphQL::Types::Boolean) }

    it do
      expect(described_class).to(
        have_graphql_argument(:datetime_value, :time_value).of_type(GraphQL::Types::ISO8601DateTime)
      )
    end

    it { expect(described_class).to have_graphql_argument(:date_value).of_type(GraphQL::Types::ISO8601Date) }
    it { expect(described_class).to have_graphql_argument(:layer_field_option_id).of_type(GraphQL::Types::ID) }
  end
end
