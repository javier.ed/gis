# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Inputs::GeocodingPointInput, type: :graphql do
  describe 'fields' do
    it do
      expect(described_class).to have_graphql_arguments(:latitude, :longitude).of_type(GraphQL::Types::Float).required
    end
  end
end
