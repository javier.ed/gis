# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Inputs::LayerInput, type: :graphql do
  describe 'fields' do
    it do
      expect(described_class).to(
        have_graphql_arguments(:name, :description, :time_interval, :suggestions).of_type(GraphQL::Types::String)
        .required
      )
    end

    it do
      expect(described_class).to(
        have_graphql_arguments(:start_at_current_time, :translate).of_type(GraphQL::Types::Boolean)
      )
    end

    it { expect(described_class).to have_graphql_argument(:fields).of_type([Inputs::Layer::FieldInput]) }
    it { expect(described_class).to have_graphql_argument(:locale).of_type(GraphQL::Types::String) }
  end
end
