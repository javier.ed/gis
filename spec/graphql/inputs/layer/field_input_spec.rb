# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Inputs::Layer::FieldInput, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:id).of_type(GraphQL::Types::ID) }
    it { expect(described_class).to have_graphql_arguments(:name).of_type(GraphQL::Types::String).required }
    it { expect(described_class).to have_graphql_argument(:field_type, :description).of_type(GraphQL::Types::String) }

    it do
      expect(described_class).to have_graphql_arguments(:required, :unique, :delete).of_type(GraphQL::Types::Boolean)
    end

    it do
      expect(described_class).to(
        have_graphql_argument(:editable).of_type(GraphQL::Types::Boolean).with_default_value(true)
      )
    end

    it { expect(described_class).to have_graphql_argument(:options).of_type([GraphQL::Types::String]) }
  end
end
