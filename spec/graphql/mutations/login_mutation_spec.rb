# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::LoginMutation, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_arguments(:login, :password).of_type(GraphQL::Types::String).required }
  end

  describe 'fields' do
    it { expect(described_class).to have_graphql_field(:user).of_type(Types::UserType).allow_null }
    it { expect(described_class).to have_graphql_field(:session).of_type(Types::SessionType).allow_null }
  end

  describe '#resolve' do
    let(:mutation) { initialize_mutation described_class }
    let(:user) { create :user_with_verified_email }

    context 'when attributes are valid' do
      it 'creates an user session' do
        expect(mutation.resolve(login: user.username, password: user.password)).to(
          include success: true, message: t('mutations.login_mutation.success')
        )
      end
    end

    context 'when password is invalid' do
      it 'fails to create an user session' do
        expect(mutation.resolve(login: user.username, password: 'wrong password')).to(
          include success: false, message: t('mutations.login_mutation.fail')
        )
      end
    end
  end
end
