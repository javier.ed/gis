# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::DeleteMarkerMutation, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:id).of_type(GraphQL::Types::ID).required }
  end

  describe '#resolve' do
    let(:marker) { create :marker }
    let(:user) { marker.user }
    let(:mutation) { initialize_mutation described_class, current_user: user }

    context 'when the ID is valid' do
      it do
        expect(mutation.resolve(id: marker.id)).to(
          include success: true, message: t('mutations.delete_marker_mutation.success')
        )
      end
    end

    context 'when the ID is invalid' do
      it do
        expect(mutation.resolve(id: 'wrong id')).to(
          include success: false, message: t('mutations.delete_marker_mutation.fail')
        )
      end
    end

    context 'when current user is the layer owner' do
      let(:user) { marker.layer.user }

      it do
        expect(mutation.resolve(id: marker.id)).to(
          include success: true, message: t('mutations.delete_marker_mutation.success')
        )
      end
    end

    context 'when current user is not authorized' do
      let(:user) { create :user }

      it do
        expect(mutation.resolve(id: marker.id)).to(
          include success: false, message: t('mutations.delete_marker_mutation.fail')
        )
      end
    end
  end
end
