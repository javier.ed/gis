# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::AddMarkerToFavoritesMutation, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:marker_id).of_type(GraphQL::Types::ID).required }
  end

  describe '#resolve' do
    let(:user) { create :user }
    let(:marker) { create :marker }
    let(:mutation) { initialize_mutation described_class, current_user: user }

    context 'when attributes are valid' do
      it do
        expect(mutation.resolve(marker_id: marker.id)).to(
          include success: true, message: t('mutations.add_marker_to_favorites_mutation.success')
        )
      end
    end

    context 'when is already in favorites' do
      let(:favorite_marker) { create :favorite_marker, user: user }
      let(:marker) { favorite_marker.marker }

      it do
        expect(mutation.resolve(marker_id: marker.id)).to(
          include success: false, message: t('mutations.add_marker_to_favorites_mutation.fail')
        )
      end
    end

    context 'when attributes are invalid' do
      it do
        expect(mutation.resolve(marker_id: 'wrong id')).to(
          include success: false, message: t('mutations.add_marker_to_favorites_mutation.fail')
        )
      end
    end
  end
end
