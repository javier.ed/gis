# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::UpdateMarkerMutation, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:id).of_type(GraphQL::Types::ID).required }
  end

  describe '#resolve' do
    let(:layer) { create :layer }
    let(:user) { layer.user }
    let(:marker) { create :marker, layer: layer, user: user }
    let(:mutation) { initialize_mutation described_class, current_user: user }
    let(:attributes) do
      OpenStruct.new(
        name: 'Some renamed marker',
        description: 'Some other description',
        remove_avatar: true,
        geocoding_point: build(:geocoding_point),
        start_at: Time.current,
        finish_at: (Time.current + 1.day),
        values: [
          OpenStruct.new(layer_field_id: layer.fields.first.id, string_value: 'Some value'),
          OpenStruct.new(
            layer_field_id: layer.fields.last.id,
            layer_field_option_id: layer.fields.last.options.first.id.to_s
          )
        ],
        locale: 'en'
      )
    end

    context 'when attributes are valid' do
      it do
        expect(mutation.resolve(id: marker.id, attributes: attributes)).to(
          include success: true, message: t('mutations.update_marker_mutation.success')
        )
      end

      it do
        expect(mutation.resolve(id: marker.id, attributes: attributes)[:marker].published_at).to be_truthy
      end
    end

    context 'when the layer is owned by other user' do
      let(:marker) { create :marker, layer: layer }
      let(:user) { marker.user }
      let(:mutation) { initialize_mutation described_class, current_user: user }

      it do
        expect(mutation.resolve(id: marker.id, attributes: attributes)).to(
          include success: true, message: t('mutations.update_marker_mutation.success')
        )
      end

      it do
        expect(mutation.resolve(id: marker.id, attributes: attributes)[:marker].published_at).to eq nil
      end
    end
  end
end
