# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::RegisterMutation, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:attributes).of_type(Inputs::UserInput).required }
  end

  describe 'fields' do
    it { expect(described_class).to have_graphql_field(:user).of_type(Types::UserType).allow_null }
    it { expect(described_class).to have_graphql_field(:session).of_type(Types::SessionType).allow_null }
  end

  describe '#resolve' do
    let(:mutation) { initialize_mutation described_class }
    let(:attributes) do
      OpenStruct.new(username: 'foo_bar', email: 'foo@bar.baz', password: '12345678', password_confirmation: '12345678')
    end

    before do
      create :layer_subscription, user: nil
    end

    context 'when attributes are valid' do
      it 'register a new user' do
        expect(mutation.resolve(attributes: attributes)).to(
          include success: true, message: t('mutations.register_mutation.success')
        )
      end
    end

    context 'when username is invalid' do
      it 'fails to register a new user' do
        attributes.username = 'wrong username'
        expect(mutation.resolve(attributes: attributes)).to include(
          success: false, message: t('mutations.register_mutation.fail')
        )
      end
    end
  end
end
