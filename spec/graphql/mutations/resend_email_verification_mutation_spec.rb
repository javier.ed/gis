# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::ResendEmailVerificationMutation, type: :graphql do
  describe '#resolve' do
    let(:user) { create :user }
    let(:mutation) { initialize_mutation described_class, current_user: user }

    context 'when the email isn\'t verified' do
      it do
        expect(mutation.resolve).to(
          include success: true, message: t('mutations.resend_email_verification_mutation.success')
        )
      end
    end

    context 'when the email is already verified' do
      it do
        user.update email_verified_at: Time.current
        expect(mutation.resolve).to(
          include success: false, message: t('mutations.resend_email_verification_mutation.fail')
        )
      end
    end
  end
end
