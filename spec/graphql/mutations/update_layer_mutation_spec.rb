# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::UpdateLayerMutation, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:id).of_type(GraphQL::Types::ID).required }
    it { expect(described_class).to have_graphql_argument(:attributes).of_type(Inputs::LayerInput).required }
  end

  describe 'fields' do
    it { expect(described_class).to have_graphql_field(:layer).of_type(Types::LayerType).allow_null }
  end

  describe '#resolve' do
    let(:layer) { create :layer }
    let(:user) { layer.user }
    let(:mutation) { initialize_mutation described_class, current_user: user }
    let(:attributes) do
      OpenStruct.new(
        name: 'Some renamed layer', description: 'Some other description', time_interval: 'any',
        start_at_current_time: false, suggestions: 'requires_approval',
        fields: [
          OpenStruct.new(id: layer.fields.first.id, name: 'Some renamed field', required: true),
          OpenStruct.new(id: layer.fields.last.id, delete: true),
          OpenStruct.new(field_type: 'string', name: 'Some other string'),
          OpenStruct.new(field_type: 'choice', name: 'Some other choice', options: ['Some option'])
        ],
        locale: 'en'
      )
    end

    context 'when attributes are valid' do
      it do
        expect(mutation.resolve(id: layer.id, attributes: attributes)).to(
          include success: true, message: t('mutations.update_layer_mutation.success')
        )
      end
    end

    context 'when the name is blank' do
      it do
        attributes.name = ''
        expect(mutation.resolve(id: layer.id, attributes: attributes)).to(
          include success: false, message: t('mutations.update_layer_mutation.fail')
        )
      end
    end

    context 'when some field name is blank' do
      it do
        attributes.fields.append(OpenStruct.new(field_type: 'string', name: ''))
        expect(mutation.resolve(id: layer.id, attributes: attributes)).to(
          include success: false, message: t('mutations.update_layer_mutation.fail')
        )
      end
    end

    context 'when some field id is invalid' do
      it do
        attributes.fields.append(OpenStruct.new(id: '0', name: 'some invalid field'))
        expect(mutation.resolve(id: layer.id, attributes: attributes)).to(
          include success: false, message: t('mutations.update_layer_mutation.fail')
        )
      end
    end
  end
end
