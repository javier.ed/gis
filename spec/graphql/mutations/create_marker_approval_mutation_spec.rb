# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::CreateMarkerApprovalMutation, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:attributes).of_type(Inputs::MarkerApprovalInput).required }
  end

  describe 'fields' do
    it { expect(described_class).to have_graphql_field(:marker_approval).of_type(Types::MarkerApprovalType).allow_null }
  end

  describe '#resolve' do
    let(:layer) { create :layer }
    let(:marker) { create :marker, layer: layer }
    let(:user) { layer.user }
    let(:mutation) { initialize_mutation described_class, current_user: user }
    let(:attributes) { OpenStruct.new(marker_id: marker.id, approved: true, comment: 'Some comment') }

    context 'when attributes are valid' do
      it 'approves the marker' do
        expect(mutation.resolve(attributes: attributes)).to(
          include success: true, message: t('mutations.create_marker_approval_mutation.success')
        )
      end

      it 'rejects the marker' do
        attributes.approved = false
        expect(mutation.resolve(attributes: attributes)).to(
          include success: true, message: t('mutations.create_marker_approval_mutation.success')
        )
      end
    end

    context 'when is rejecting with an empty comment' do
      it do
        attributes.approved = false
        attributes.comment = ''
        expect(mutation.resolve(attributes: attributes)).to(
          include success: false, message: t('mutations.create_marker_approval_mutation.fail')
        )
      end
    end

    context 'when the marker is already approved' do
      let(:marker) { create :marker, layer: layer, published_at: Time.current }

      it 'is allowed to reject the marker' do
        attributes.approved = false
        expect(mutation.resolve(attributes: attributes)).to(
          include success: true, message: t('mutations.create_marker_approval_mutation.success')
        )
      end

      it 'fails to approve' do
        expect(mutation.resolve(attributes: attributes)).to(
          include success: false, message: t('mutations.create_marker_approval_mutation.fail')
        )
      end
    end

    context 'when the marker is invalid' do
      it do
        attributes.marker_id = 'wrong id'
        expect(mutation.resolve(attributes: attributes)).to(
          include success: false, message: t('mutations.create_marker_approval_mutation.fail')
        )
      end
    end

    context 'when the current user is invalid' do
      let(:user) { create :user }

      it do
        expect(mutation.resolve(attributes: attributes)).to(
          include success: false, message: t('mutations.create_marker_approval_mutation.fail')
        )
      end
    end

    context 'when the marker is from the owner of the layer' do
      let(:marker) { create :marker, layer: layer, user: user }

      it do
        expect(mutation.resolve(attributes: attributes)).to(
          include success: false, message: t('mutations.create_marker_approval_mutation.fail')
        )
      end
    end
  end
end
