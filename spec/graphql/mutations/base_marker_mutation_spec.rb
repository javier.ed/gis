# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::BaseMarkerMutation, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:attributes).of_type(Inputs::MarkerInput).required }
  end

  describe 'fields' do
    it { expect(described_class).to have_graphql_field(:marker).of_type(Types::MarkerType).allow_null }
  end
end
