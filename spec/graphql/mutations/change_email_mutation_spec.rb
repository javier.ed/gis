# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::ChangeEmailMutation, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_arguments(:password, :email).of_type(GraphQL::Types::String).required }
  end

  describe '#resolve' do
    let(:password) { '12345678' }
    let(:user) { create :user, password: password, password_confirmation: password }
    let(:mutation) { initialize_mutation described_class, current_user: user }
    let(:email) { 'new@email.address' }

    context 'when all data is valid' do
      it do
        expect(mutation.resolve(password: password, email: email)).to(
          include success: true, message: t('mutations.change_email_mutation.success')
        )
      end
    end

    context 'when the password is invalid' do
      it do
        expect(mutation.resolve(password: 'wrong password', email: email)).to(
          include success: false, message: t('mutations.change_email_mutation.fail')
        )
      end
    end

    context 'when email is not changing' do
      it do
        expect(mutation.resolve(password: password, email: user.email)).to(
          include success: false, message: t('mutations.change_email_mutation.fail')
        )
      end
    end
  end
end
