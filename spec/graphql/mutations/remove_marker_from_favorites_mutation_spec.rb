# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::RemoveMarkerFromFavoritesMutation, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:id).of_type(GraphQL::Types::ID).required }
  end

  describe '#resolve' do
    let(:favorite_marker) { create :favorite_marker }
    let(:user) { favorite_marker.user }
    let(:mutation) { initialize_mutation described_class, current_user: user }

    context 'when the id is valid' do
      it do
        expect(mutation.resolve(id: favorite_marker.id)).to(
          include success: true, message: t('mutations.remove_marker_from_favorites_mutation.success')
        )
      end
    end

    context 'when the id is invalid' do
      it do
        expect(mutation.resolve(id: 'wrong id')).to(
          include success: false, message: t('mutations.remove_marker_from_favorites_mutation.fail')
        )
      end
    end
  end
end
