# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::CreateLayerMutation, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:attributes).of_type(Inputs::LayerInput).required }
  end

  describe 'fields' do
    it { expect(described_class).to have_graphql_field(:layer).of_type(Types::LayerType).allow_null }
  end

  describe '#resolve' do
    let(:user) { create :user }
    let(:mutation) { initialize_mutation described_class, current_user: user }
    let(:attributes) do
      OpenStruct.new(
        name: 'Some name', description: 'Some description', time_interval: 'any', start_at_current_time: false,
        suggestions: 'requires_approval',
        fields: [
          OpenStruct.new(field_type: 'string', name: 'Some string'),
          OpenStruct.new(field_type: 'choice', name: 'Some choice', options: ['Some option'])
        ],
        locale: 'en',
        translate: true
      )
    end

    context 'when attributes are valid' do
      it do
        expect(mutation.resolve(attributes: attributes)).to(
          include success: true, message: t('mutations.create_layer_mutation.success')
        )
      end
    end

    context 'when there are no fields' do
      it do
        attributes.fields = nil
        expect(mutation.resolve(attributes: attributes)).to(
          include success: true, message: t('mutations.create_layer_mutation.success')
        )
      end
    end

    context 'when the name is blank' do
      it do
        attributes.name = ''
        expect(mutation.resolve(attributes: attributes)).to(
          include success: false, message: t('mutations.create_layer_mutation.fail')
        )
      end
    end

    context 'when some field name is blank' do
      it do
        attributes.fields.append(OpenStruct.new(field_type: 'string', name: ''))
        expect(mutation.resolve(attributes: attributes)).to(
          include success: false, message: t('mutations.create_layer_mutation.fail')
        )
      end
    end
  end
end
