# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::ChangePasswordMutation, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:password).of_type(GraphQL::Types::String).required }
    it { expect(described_class).to have_graphql_argument(:attributes).of_type(Inputs::PasswordInput).required }
  end

  describe '#resolve' do
    let(:password) { '12345678' }
    let(:user) { create :user, password: password, password_confirmation: password }
    let(:mutation) { described_class.new object: nil, field: nil, context: custom_context(current_user: user) }
    let(:attributes) { OpenStruct.new password: '87654321', password_confirmation: '87654321' }

    context 'when all data is valid' do
      it 'changes password' do
        expect(mutation.resolve(password: password, attributes: attributes)).to(
          include success: true, message: t('mutations.change_password_mutation.success')
        )
      end
    end

    context 'when current password is invalid' do
      it 'fails to change password' do
        expect(mutation.resolve(password: 'wrong password', attributes: attributes)).to(
          include success: false, message: t('mutations.change_password_mutation.fail')
        )
      end
    end
  end
end
