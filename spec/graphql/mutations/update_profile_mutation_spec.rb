# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::UpdateProfileMutation, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:attributes).of_type(Inputs::ProfileInput).required }
  end

  describe '#resolve' do
    let(:user) { create :user }
    let(:mutation) { initialize_mutation described_class, current_user: user }
    let(:attributes) do
      OpenStruct.new(
        name: 'Some name',
        bio: 'Some text',
        country_id: 'VE',
        birthdate: Date.current,
        avatar: Rack::Test::UploadedFile.new('spec/fixtures/image-256x256.png')
      )
    end

    context 'when attributes are valid' do
      it do
        expect(mutation.resolve(attributes: attributes)).to(
          include success: true, message: t('mutations.update_profile_mutation.success')
        )
      end
    end

    context 'when the country code is invalid' do
      it do
        attributes.country_id = 'wrong code'
        expect(mutation.resolve(attributes: attributes)).to(
          include success: false, message: t('mutations.update_profile_mutation.fail')
        )
      end
    end

    context 'when the birthdate is invalid' do
      it do
        attributes.birthdate = Date.current + 1.day
        expect(mutation.resolve(attributes: attributes)).to(
          include success: false, message: t('mutations.update_profile_mutation.fail')
        )
      end
    end

    context 'when the avatar image is invalid' do
      it do
        attributes.avatar = Rack::Test::UploadedFile.new('spec/fixtures/text_file.txt')
        expect(mutation.resolve(attributes: attributes)).to(
          include success: false, message: t('mutations.update_profile_mutation.fail')
        )
      end
    end
  end
end
