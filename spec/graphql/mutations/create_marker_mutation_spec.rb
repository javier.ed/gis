# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::CreateMarkerMutation, type: :graphql do
  describe '#resolve' do
    let(:layer) { create :layer }
    let(:user) { layer.user }
    let(:mutation) { initialize_mutation described_class, current_user: user }
    let(:attributes) do
      OpenStruct.new(
        layer_id: layer.id,
        geocoding_point: build(:geocoding_point),
        name: 'Some marker',
        description: 'Some description',
        avatar: Rack::Test::UploadedFile.new('spec/fixtures/image-256x256.png'),
        start_at: Time.current,
        finish_at: (Time.current + 1.day),
        values: [
          OpenStruct.new(layer_field_id: layer.fields.first.id, string_value: 'Some value'),
          OpenStruct.new(
            layer_field_id: layer.fields.last.id,
            layer_field_option_id: layer.fields.last.options.first.id.to_s
          )
        ],
        locale: 'en',
        translate: true
      )
    end

    context 'when attributes are valid' do
      it do
        expect(mutation.resolve(attributes: attributes)).to(
          include success: true, message: t('mutations.create_marker_mutation.success')
        )
      end

      it do
        expect(mutation.resolve(attributes: attributes)[:marker].published_at).to be_truthy
      end
    end

    context 'when the layer is owned by other user' do
      let(:user) { create :user }

      it do
        expect(mutation.resolve(attributes: attributes)).to(
          include success: true, message: t('mutations.create_marker_mutation.success')
        )
      end

      it do
        expect(mutation.resolve(attributes: attributes)[:marker].published_at).to eq nil
      end
    end

    context 'when the name is blank' do
      it do
        attributes.name = ''
        expect(mutation.resolve(attributes: attributes)).to(
          include success: false, message: t('mutations.create_marker_mutation.fail')
        )
      end
    end

    context 'when a required value is blank' do
      it do
        attributes.values.first.string_value = ''
        expect(mutation.resolve(attributes: attributes)).to(
          include success: false, message: t('mutations.create_marker_mutation.fail')
        )
      end
    end

    context 'when an option value is invalid' do
      it do
        attributes.values.last.layer_field_option_id = '0'
        expect(mutation.resolve(attributes: attributes)).to(
          include success: false, message: t('mutations.create_marker_mutation.fail')
        )
      end
    end

    context 'when the time range is backwards' do
      it do
        attributes.start_at = attributes.start_at + 1.day
        attributes.finish_at = attributes.finish_at - 1.day
        expect(mutation.resolve(attributes: attributes)).to(
          include success: false, message: t('mutations.create_marker_mutation.fail')
        )
      end
    end

    context 'when the avatar image is invalid' do
      it do
        attributes.avatar = Rack::Test::UploadedFile.new('spec/fixtures/text_file.txt')
        expect(mutation.resolve(attributes: attributes)).to(
          include success: false, message: t('mutations.create_marker_mutation.fail')
        )
      end
    end

    context 'when the layer allow to publish directly' do
      let(:layer) { create :layer, suggestions: 'publish_directly' }
      let(:user) { create :user }

      it do
        expect(mutation.resolve(attributes: attributes)).to(
          include success: true, message: t('mutations.create_marker_mutation.success')
        )
      end

      it do
        expect(mutation.resolve(attributes: attributes)[:marker].published_at).to be_truthy
      end
    end

    context 'when the layer doesn\'t allow suggestions and current user is the layer owner' do
      let(:layer) { create :layer, suggestions: 'deny' }

      it do
        expect(mutation.resolve(attributes: attributes)).to(
          include success: true, message: t('mutations.create_marker_mutation.success')
        )
      end
    end

    context 'when the layer doesn\'t allow suggestions and current user is invalid' do
      let(:layer) { create :layer, suggestions: 'deny' }
      let(:user) { create :user }

      it do
        expect(mutation.resolve(attributes: attributes)).to(
          include success: false, message: t('mutations.create_marker_mutation.fail')
        )
      end
    end

    context 'when the layer have a time interval' do
      let(:layer) { create :layer, time_interval: '1 day' }

      it 'doesn\'t read attribute finish_at' do
        attributes.start_at = attributes.start_at + 1.day
        attributes.finish_at = attributes.finish_at - 1.day
        expect(mutation.resolve(attributes: attributes)).to(
          include success: true, message: t('mutations.create_marker_mutation.success')
        )
      end
    end

    context 'when start at current time' do
      let(:layer) { create :layer, start_at_current_time: true }

      it 'doesn\'t read attribute start_at' do
        attributes.start_at = attributes.start_at + 2.days
        expect(mutation.resolve(attributes: attributes)).to(
          include success: true, message: t('mutations.create_marker_mutation.success')
        )
      end

      it 'fails if attributes finish_at is less than current time' do
        attributes.start_at = nil
        attributes.finish_at = attributes.finish_at - 2.days
        expect(mutation.resolve(attributes: attributes)).to(
          include success: false, message: t('mutations.create_marker_mutation.fail')
        )
      end
    end
  end
end
