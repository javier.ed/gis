# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::SubscribeToLayerMutation, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:layer_id).of_type(GraphQL::Types::ID).required }
  end

  describe '#resolve' do
    let(:user) { create :user }
    let(:layer) { create :layer }
    let(:mutation) { initialize_mutation described_class, current_user: user }

    context 'when attributes are valid' do
      it do
        expect(mutation.resolve(layer_id: layer.id)).to(
          include success: true, message: t('mutations.subscribe_to_layer_mutation.success')
        )
      end
    end

    context 'when is already subscribed' do
      let(:layer_subscription) { create :layer_subscription, user: user }
      let(:layer) { layer_subscription.layer }

      it do
        expect(mutation.resolve(layer_id: layer.id)).to(
          include success: false, message: t('mutations.subscribe_to_layer_mutation.fail')
        )
      end
    end

    context 'when attributes are invalid' do
      it do
        expect(mutation.resolve(layer_id: 'wrong id')).to(
          include success: false, message: t('mutations.subscribe_to_layer_mutation.fail')
        )
      end
    end
  end
end
