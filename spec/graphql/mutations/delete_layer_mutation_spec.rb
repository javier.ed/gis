# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::DeleteLayerMutation, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:id).of_type(GraphQL::Types::ID).required }
  end

  describe '#resolve' do
    let(:layer) { create :layer }
    let(:user) { layer.user }
    let(:mutation) { initialize_mutation described_class, current_user: user }

    context 'when the id is valid' do
      it do
        expect(mutation.resolve(id: layer.id)).to(
          include success: true, message: t('mutations.delete_layer_mutation.success')
        )
      end
    end

    context 'when the id is invalid' do
      it do
        expect(mutation.resolve(id: 'wrong id')).to(
          include success: false, message: t('mutations.delete_layer_mutation.fail')
        )
      end
    end
  end
end
