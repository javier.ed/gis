# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::VerifyEmailMutation, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:confirmation_code).of_type(GraphQL::Types::String).required }
  end

  describe '#resolve' do
    let(:user) { create :user }
    let(:otp) { create :otp, user: user, action: 'verify_email' }
    let(:mutation) { initialize_mutation described_class, current_user: user }

    context 'when the confirmation code is valid' do
      it 'verifies the emails address' do
        expect(mutation.resolve(confirmation_code: otp.password)).to(
          include success: true, message: t('mutations.verify_email_mutation.success')
        )
      end
    end

    context 'when the confirmation code is invalid' do
      it 'fails to verify the email address' do
        expect(mutation.resolve(confirmation_code: 'wrong code')).to(
          include success: false, message: t('mutations.verify_email_mutation.fail')
        )
      end
    end
  end
end
