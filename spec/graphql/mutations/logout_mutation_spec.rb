# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::LogoutMutation, type: :graphql do
  describe '#resolve' do
    let(:user) { create :user }
    let(:mutation) { initialize_mutation described_class, current_user: user }

    it 'logouts user' do
      expect(mutation.resolve).to include success: true, message: t('mutations.logout_mutation.success')
    end
  end
end
