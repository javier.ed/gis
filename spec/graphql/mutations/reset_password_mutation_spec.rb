# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::ResetPasswordMutation, type: :graphql do
  describe 'arguments' do
    it do
      expect(described_class).to(
        have_graphql_arguments(:login, :confirmation_code).of_type(GraphQL::Types::String).required
      )
    end

    it { expect(described_class).to have_graphql_argument(:attributes).of_type(Inputs::PasswordInput).required }
  end

  describe '#resolve' do
    let(:mutation) { initialize_mutation described_class }
    let(:user) { create :user }
    let(:otp) { user.otps.create action: 'reset_password' }
    let(:attributes) { OpenStruct.new password: '87654321', password_confirmation: '87654321' }

    context 'when arguments are valid' do
      it 'changes password' do
        expect(mutation.resolve(login: user.username, confirmation_code: otp.password, attributes: attributes)).to(
          include success: true, message: t('mutations.reset_password_mutation.success')
        )
      end
    end

    context 'when the confirmation code is invalid' do
      it 'fails to change password' do
        expect(mutation.resolve(login: user.username, confirmation_code: 'wrong code', attributes: attributes)).to(
          include success: false, message: t('mutations.reset_password_mutation.fail')
        )
      end
    end
  end
end
