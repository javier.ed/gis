# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::CloneMarkerMutation, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:id).of_type(GraphQL::Types::ID).required }
  end

  describe 'fields' do
    it { expect(described_class).to have_graphql_field(:marker).of_type(Types::MarkerType).allow_null }
  end

  describe '#resolve' do
    let(:layer) { create :layer }
    let(:marker) { create :marker, layer: layer }
    let(:user) { create :user }
    let(:mutation) { initialize_mutation described_class, current_user: user }

    context 'when the ID is valid' do
      it do
        expect(mutation.resolve(id: marker.id)).to(
          include success: true, message: t('mutations.clone_marker_mutation.success')
        )
      end
    end

    context 'when the ID is invalid' do
      it do
        expect(mutation.resolve(id: 'wrong id')).to(
          include success: false, message: t('mutations.clone_marker_mutation.fail')
        )
      end
    end

    context 'when the layer doesn\'t allow suggestions and current user is the layer owner' do
      let(:layer) { create :layer, suggestions: 'deny' }
      let(:user) { layer.user }

      it do
        expect(mutation.resolve(id: marker.id)).to(
          include success: true, message: t('mutations.clone_marker_mutation.success')
        )
      end
    end

    context 'when the layer doesn\'t allow suggestions and current user is invalid' do
      let(:layer) { create :layer, suggestions: 'deny' }

      it do
        expect(mutation.resolve(id: marker.id)).to(
          include success: false, message: t('mutations.clone_marker_mutation.fail')
        )
      end
    end
  end
end
