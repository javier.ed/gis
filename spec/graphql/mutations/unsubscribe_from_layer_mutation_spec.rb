# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Mutations::UnsubscribeFromLayerMutation, type: :graphql do
  describe 'arguments' do
    it { expect(described_class).to have_graphql_argument(:id).of_type(GraphQL::Types::ID).required }
  end

  describe '#resolve' do
    let(:layer_subscription) { create :layer_subscription }
    let(:user) { layer_subscription.user }
    let(:mutation) { initialize_mutation described_class, current_user: user }

    context 'when the id is valid' do
      it do
        expect(mutation.resolve(id: layer_subscription.id)).to(
          include success: true, message: t('mutations.unsubscribe_from_layer_mutation.success')
        )
      end
    end

    context 'when the id is invalid' do
      it do
        expect(mutation.resolve(id: 'wrong id')).to(
          include success: false, message: t('mutations.unsubscribe_from_layer_mutation.fail')
        )
      end
    end
  end
end
