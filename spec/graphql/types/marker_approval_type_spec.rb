# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::MarkerApprovalType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_global_id_field :id }

    it { expect(described_class).to have_graphql_field(:user).of_type(Types::UserType).allow_null }
    it { expect(described_class).to have_graphql_field(:marker).of_type(Types::MarkerType).allow_null }
    it { expect(described_class).to have_graphql_field(:approved).of_type(GraphQL::Types::Boolean).allow_null }
    it { expect(described_class).to have_graphql_field(:comment).of_type(GraphQL::Types::String).allow_null }
  end
end
