# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::MarkerType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_global_id_field :id }

    it do
      expect(described_class).to(
        have_graphql_fields(:created_at, :updated_at, :published_at, :start_at, :finish_at)
        .of_type(GraphQL::Types::ISO8601DateTime).allow_null
      )
    end

    it do
      expect(described_class).to(
        have_graphql_fields(:name, :description, :avatar_url).of_type(GraphQL::Types::String).allow_null
      )
    end

    it { expect(described_class).to have_graphql_field(:layer).of_type(Types::LayerType).allow_null }
    it { expect(described_class).to have_graphql_field(:user).of_type(Types::UserType).allow_null }

    it do
      expect(described_class).to have_graphql_field(:geocoding_point).of_type(Types::GeocodingPointType).allow_null
    end

    it do
      expect(described_class).to(
        have_graphql_field(:values).of_type(Types::Marker::ValueType.connection_type).allow_null
      )
    end

    it { expect(described_class).to have_graphql_field(:published).of_type(GraphQL::Types::Boolean).allow_null }
    it { expect(described_class).to have_graphql_field(:approval).of_type(Types::MarkerApprovalType).allow_null }

    it do
      expect(described_class).to(
        have_graphql_field(:current_user_favorite).of_type(Types::FavoriteMarkerType).allow_null
      )
    end
  end
end
