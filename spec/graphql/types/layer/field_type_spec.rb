# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::Layer::FieldType, type: :graphql do
  it { expect(described_class).to have_graphql_global_id_field :id }

  it do
    expect(described_class).to(
      have_graphql_fields(:field_type, :name, :description).of_type(GraphQL::Types::String).allow_null
    )
  end

  it do
    expect(described_class).to(
      have_graphql_fields(:required, :unique, :editable).of_type(GraphQL::Types::Boolean).allow_null
    )
  end

  it do
    expect(described_class).to(
      have_graphql_field(:options).of_type(Types::Layer::FieldOptionType.connection_type).allow_null
    )
  end
end
