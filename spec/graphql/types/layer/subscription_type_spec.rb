# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::Layer::SubscriptionType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_global_id_field :id }
    it { expect(described_class).to have_graphql_field(:layer).of_type(Types::LayerType).allow_null }
  end
end
