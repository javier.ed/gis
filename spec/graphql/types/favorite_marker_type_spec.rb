# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::FavoriteMarkerType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_global_id_field :id }
    it { expect(described_class).to have_graphql_field(:marker).of_type(Types::MarkerType).allow_null }
  end
end
