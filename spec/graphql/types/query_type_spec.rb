# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::QueryType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_field(:info).of_type(Types::InfoType).allow_null }
    it { expect(described_class).to have_graphql_fields(:current_user, :user).of_type(Types::UserType).allow_null }

    it { expect(described_class).to have_graphql_field(:layer).of_type(Types::LayerType).allow_null }
    it { expect(described_class).to have_graphql_field(:layers).of_type(Types::LayerType.connection_type).allow_null }

    it do
      expect(described_class).to(
        have_graphql_field(:geocoding_point, :reverse_geocode).of_type(Types::GeocodingPointType).allow_null
      )
    end

    it do
      expect(described_class).to(
        have_graphql_fields(:search_geocoding_points, :locate_geocoding_points)
        .of_type(Types::GeocodingPointType.connection_type).allow_null
      )
    end

    it { expect(described_class).to have_graphql_field(:marker).of_type(Types::MarkerType).allow_null }

    it do
      expect(described_class).to(
        have_graphql_fields(:search_markers, :locate_markers).of_type(Types::MarkerType.connection_type).allow_null
      )
    end

    it do
      expect(described_class).to(
        have_graphql_field(:favorite_markers, :locate_favorite_markers)
        .of_type(Types::FavoriteMarkerType.connection_type).allow_null
      )
    end

    it do
      expect(described_class).to(
        have_graphql_field(:layer_subscriptions).of_type(Types::Layer::SubscriptionType.connection_type).allow_null
      )
    end

    it do
      expect(described_class).to have_graphql_field(:all_countries).of_type([Types::CountryType]).allow_null
    end
  end
end
