# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::CountryType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_global_id_field(:id) }

    it { expect(described_class).to have_graphql_field(:name).of_type(GraphQL::Types::String).allow_null }
  end
end
