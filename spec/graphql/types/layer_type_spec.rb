# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::LayerType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_global_id_field :id }

    it do
      expect(described_class).to(
        have_graphql_fields(:created_at, :updated_at).of_type(GraphQL::Types::ISO8601DateTime).allow_null
      )
    end

    it do
      expect(described_class).to(
        have_graphql_fields(:name, :description, :time_interval, :suggestions).of_type(GraphQL::Types::String)
        .allow_null
      )
    end

    it do
      expect(described_class).to have_graphql_field(:start_at_current_time).of_type(GraphQL::Types::Boolean).allow_null
    end

    it do
      expect(described_class).to have_graphql_field(:fields).of_type(Types::Layer::FieldType.connection_type).allow_null
    end

    it { expect(described_class).to have_graphql_field(:user).of_type(Types::UserType).allow_null }

    it do
      expect(described_class).to(
        have_graphql_fields(:markers, :pending_markers).of_type(Types::MarkerType.connection_type).allow_null
      )
    end

    it do
      expect(described_class).to(
        have_graphql_field(:subscriptions).of_type(Types::Layer::SubscriptionType.connection_type).allow_null
      )
    end

    it do
      expect(described_class).to(
        have_graphql_fields(:subscriptions_count, :time_interval_long).of_type(GraphQL::Types::Int).allow_null
      )
    end

    it do
      expect(described_class).to(
        have_graphql_field(:current_user_subscription).of_type(Types::Layer::SubscriptionType).allow_null
      )
    end
  end
end
