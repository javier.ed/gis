# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::User::ProfileType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_global_id_field(:id) }

    it do
      expect(described_class).to(
        have_graphql_fields(:created_at, :updated_at).of_type(GraphQL::Types::ISO8601DateTime).allow_null
      )
    end

    it do
      expect(described_class).to(
        have_graphql_fields(:name, :bio, :country_id, :avatar_url).of_type(GraphQL::Types::String).allow_null
      )
    end

    it { expect(described_class).to have_graphql_field(:birthdate).of_type(GraphQL::Types::ISO8601Date).allow_null }

    it { expect(described_class).to have_graphql_field(:country).of_type(Types::CountryType).allow_null }
  end
end
