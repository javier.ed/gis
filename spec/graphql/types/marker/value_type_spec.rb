# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::Marker::ValueType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_global_id_field :id }

    it do
      expect(described_class).to(
        have_graphql_fields(:created_at, :updated_at).of_type(GraphQL::Types::ISO8601DateTime).allow_null
      )
    end

    it { expect(described_class).to have_graphql_field(:layer_field).of_type(Types::Layer::FieldType).allow_null }

    it do
      expect(described_class).to(
        have_graphql_fields(:string_value, :text_value).of_type(GraphQL::Types::String).allow_null
      )
    end

    it { expect(described_class).to have_graphql_field(:integer_value).of_type(GraphQL::Types::Int).allow_null }
    it { expect(described_class).to have_graphql_field(:float_value).of_type(GraphQL::Types::Float).allow_null }
    it { expect(described_class).to have_graphql_field(:boolean_value).of_type(GraphQL::Types::Boolean).allow_null }

    it do
      expect(described_class).to(
        have_graphql_field(:datetime_value, :time_value).of_type(GraphQL::Types::ISO8601DateTime).allow_null
      )
    end

    it { expect(described_class).to have_graphql_field(:date_value).of_type(GraphQL::Types::ISO8601Date).allow_null }

    it do
      expect(described_class).to(
        have_graphql_field(:layer_field_option).of_type(Types::Layer::FieldOptionType).allow_null
      )
    end
  end
end
