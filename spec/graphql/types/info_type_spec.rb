# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::InfoType, type: :graphql do
  describe 'fields' do
    it { expect(described_class).to have_graphql_field(:server_version).of_type(GraphQL::Types::String).allow_null }

    it do
      expect(described_class).to have_graphql_field(:server_date).of_type(GraphQL::Types::ISO8601DateTime).allow_null
    end

    it { expect(described_class).to have_graphql_field(:scopes).of_type([GraphQL::Types::String]).allow_null }
  end
end
