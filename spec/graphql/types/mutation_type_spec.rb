# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Types::MutationType, type: :graphql do
  it { expect(described_class).to have_graphql_field(:change_email).with_mutation(Mutations::ChangeEmailMutation) }

  it do
    expect(described_class).to have_graphql_field(:change_password).with_mutation(Mutations::ChangePasswordMutation)
  end

  it { expect(described_class).to have_graphql_field(:clone_marker).with_mutation(Mutations::CloneMarkerMutation) }
  it { expect(described_class).to have_graphql_field(:login).with_mutation(Mutations::LoginMutation) }
  it { expect(described_class).to have_graphql_field(:logout).with_mutation(Mutations::LogoutMutation) }
  it { expect(described_class).to have_graphql_field(:register).with_mutation(Mutations::RegisterMutation) }

  it do
    expect(described_class).to(
      have_graphql_field(:request_password_reset).with_mutation(Mutations::RequestPasswordResetMutation)
    )
  end

  it do
    expect(described_class).to(
      have_graphql_field(:resend_email_verification).with_mutation(Mutations::ResendEmailVerificationMutation)
    )
  end

  it { expect(described_class).to have_graphql_field(:reset_password).with_mutation(Mutations::ResetPasswordMutation) }

  it { expect(described_class).to have_graphql_field(:update_profile).with_mutation(Mutations::UpdateProfileMutation) }
  it { expect(described_class).to have_graphql_field(:verify_email).with_mutation(Mutations::VerifyEmailMutation) }

  it { expect(described_class).to have_graphql_field(:create_layer).with_mutation(Mutations::CreateLayerMutation) }
  it { expect(described_class).to have_graphql_field(:update_layer).with_mutation(Mutations::UpdateLayerMutation) }
  it { expect(described_class).to have_graphql_field(:delete_layer).with_mutation(Mutations::DeleteLayerMutation) }

  it { expect(described_class).to have_graphql_field(:create_marker).with_mutation(Mutations::CreateMarkerMutation) }
  it { expect(described_class).to have_graphql_field(:update_marker).with_mutation(Mutations::UpdateMarkerMutation) }
  it { expect(described_class).to have_graphql_field(:delete_marker).with_mutation(Mutations::DeleteMarkerMutation) }

  it do
    expect(described_class).to(
      have_graphql_field(:create_marker_approval).with_mutation(Mutations::CreateMarkerApprovalMutation)
    )
  end

  it do
    expect(described_class).to(
      have_graphql_field(:add_marker_to_favorites).with_mutation(Mutations::AddMarkerToFavoritesMutation)
    )
  end

  it do
    expect(described_class).to(
      have_graphql_field(:remove_marker_from_favorites).with_mutation(Mutations::RemoveMarkerFromFavoritesMutation)
    )
  end

  it do
    expect(described_class).to(
      have_graphql_field(:subscribe_to_layer).with_mutation(Mutations::SubscribeToLayerMutation)
    )
  end

  it do
    expect(described_class).to(
      have_graphql_field(:unsubscribe_from_layer).with_mutation(Mutations::UnsubscribeFromLayerMutation)
    )
  end
end
