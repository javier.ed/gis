# frozen_string_literal: true

# User mailer preview
class UserMailerPreview < ActionMailer::Preview
  def welcome_mail
    UserMailer.with(to: 'foo@bar.baz', username: 'some_guy').welcome_mail
  end
end
