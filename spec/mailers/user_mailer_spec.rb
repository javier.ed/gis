# frozen_string_literal: true

require 'rails_helper'

RSpec.describe UserMailer, type: :mailer do
  let(:email) { 'foo@bar.baz' }
  let(:username) { 'some_guy' }

  describe 'welcome mail' do
    let(:mail) do
      described_class.with(to: email, username: username).welcome_mail
    end

    it { expect(mail.to).to eq [email] }
    it { expect(mail.subject).to eq t('user_mailer.welcome_mail.subject') }
    it { expect(mail.html_part.body).to match t('user_mailer.welcome_mail.first_paragraph') }
    it { expect(mail.text_part.body).to match t('user_mailer.welcome_mail.first_paragraph') }
  end
end
