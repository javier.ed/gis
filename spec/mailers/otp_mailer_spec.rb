# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OtpMailer, type: :mailer do
  let(:email) { 'foo@bar.baz' }
  let(:username) { 'some_guy' }
  let(:password) { 'L0R3M1P5UM' }
  let(:action) { :reset_password }

  describe 'password mail' do
    let(:mail) do
      described_class.with(to: email, username: username, password: password, action: :reset_password).password_mail
    end

    it { expect(mail.to).to eq [email] }
    it { expect(mail.subject).to eq t('otp_mailer.password_mail.subject') }
    it { expect(mail.body.encoded).to match t('mailers.hello', username: username) }
  end
end
