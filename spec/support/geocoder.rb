# frozen_string_literal: true

Geocoder.configure(lookup: :test)

Geocoder::Lookup::Test.add_stub('Cumaná', JSON.parse(File.open('spec/fixtures/geocoder_results.json').read))
Geocoder::Lookup::Test.add_stub(
  [10.4494466, -64.1630154], JSON.parse(File.open('spec/fixtures/geocoder_results.json').read)
)

Geocoder::Lookup::Test.set_default_stub([])
