# frozen_string_literal: true

require 'webmock/rspec'
require 'settings'

RSpec.configure do |config|
  config.before do
    uri_template = Addressable::Template.new "#{Settings.apertium.apy_translate_url}{?q,langpair}"
    stub_request(:any, uri_template)
      .to_return(
        body: {
          responseData: { translatedText: 'Some translation' }, responseDetails: nil, responseStatus: 200
        }.to_json
      )
  end
end
