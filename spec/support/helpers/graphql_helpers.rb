# frozen_string_literal: true

# GraphQL helpers for RSpec
module GraphqlHelpers
  # @param current_user [User]
  # @return [OpenStruct]
  def custom_context(current_user: nil)
    current_client_app = create(:client_app)
    current_session = current_client_app.sessions.create!(user: current_user) if current_user.present?
    OpenStruct.new(
      current_client_app: current_client_app,
      current_session: current_session,
      current_user: current_user
    )
  end

  # @param mutation_class [Mutations::BaseMutation.class]
  # @param current_user [User, nil]
  # @return [Mutations::BaseMutation]
  def initialize_mutation(mutation_class, current_user: nil)
    context = custom_context(current_user: current_user)
    mutation_class.new object: nil, field: nil, context: context
  end
end
