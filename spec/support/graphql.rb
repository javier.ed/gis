# frozen_string_literal: true

require './spec/support/matchers/graphql_matchers'
require './spec/support/helpers/graphql_helpers'

RSpec.configure do |config|
  config.include GraphqlMatchers, type: :graphql
  config.include GraphqlHelpers, type: :graphql
end
