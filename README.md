# Bululú (Server)

[![pipeline status](https://gitlab.com/bululu/bululu/badges/master/pipeline.svg)](https://gitlab.com/bululu/bululu/commits/master)

[![coverage report](https://gitlab.com/bululu/bululu/badges/master/coverage.svg)](https://gitlab.com/bululu/bululu/commits/master)

## Version

### 0.3-snapshot

## Requirements

* Git 2.20+
* Ruby 2.7
* PostgreSQL 11.7+
* PostGIS 2.4+
* Redis 5.0+

## Installation instructions

### Basic configuration

```bash
cp config/cable.example.yml config/cable.yml
cp config/database.example.yml config/database.yml
cp config/settings.example.yml config/settings.yml
cp config/sidekiq.example.yml config/sidekiq.yml
cp config/storage.example.yml config/storage.yml
```

### Install dependencies

```bash
bundle install
```

### Create database and load schema

```bash
bundle exec rails db:create
bundle exec rails db:schema:load
```

### Development deployment

```bash
bundle exec rails server
```
