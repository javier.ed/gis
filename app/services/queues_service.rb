# frozen_string_literal: true

# Settings module
class QueuesService < ApplicationService
  def initialize(action:, scope: 'all', sleep: 1)
    @sleep = sleep

    number = redis.incr("queue_counter_for_#{action}_at_#{scope}")

    @previous = "queue_turn_for_#{action}_at_#{scope}_is_#{number - 1}"
    @current = "queue_turn_for_#{action}_at_#{scope}_is_#{number}"

    redis.set(@current, true)
  end

  def call
    wait_turn

    if block_given?
      yield
    else
      self
    end
  rescue StandardError => e
    Rails.logger.error e.message
    Rails.logger.error e.backtrace.join("\n")
  ensure
    finish_turn if block_given?
  end

  def finish_turn
    redis.del(@current)
  end

  private

  def wait_turn
    return true if redis.get(@previous).nil?

    sleep(@sleep)
    wait_turn
  end

  def redis
    return @redis if @redis.present?

    @redis = Redis.new(url: Settings.queues.redis_url)
  end
end
