# frozen_string_literal: true

module Types
  # Layer type
  class LayerType < Types::BaseObject
    global_id_field :id
    field :created_at, GraphQL::Types::ISO8601DateTime, null: true
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: true

    field :name, String, null: true
    def name
      object.name || object.name_translations.first[1]
    end

    field :description, String, null: true
    def description
      object.description || object.description_translations.first[1]
    end

    field :time_interval, String, null: true
    field :start_at_current_time, Boolean, null: true
    field :suggestions, String, null: true
    field :fields, Types::Layer::FieldType.connection_type, null: true
    field :user, Types::UserType, null: true
    field :markers, Types::MarkerType.connection_type, null: true
    field :pending_markers, Types::MarkerType.connection_type, null: true

    field :subscriptions, Types::Layer::SubscriptionType.connection_type, null: true
    def subscriptions
      object.subscriptions.where('user_id IS NOT NULL')
    end

    field :subscriptions_count, Int, null: true
    def subscriptions_count
      object.subscriptions.where('user_id IS NOT NULL').count
    end

    field :current_user_subscription, Types::Layer::SubscriptionType, null: true
    def current_user_subscription
      return nil if context.current_user.blank?

      object.subscriptions.find_by(user: context.current_user)
    end

    field :time_interval_long, Int, null: true
    def time_interval_long
      ChronicDuration.parse(object.time_interval)
    rescue StandardError => e
      logger.error e.message
      logger.error e.backtrace.join("\n")
      nil
    end
  end
end
