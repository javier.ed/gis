# frozen_string_literal: true

module Types
  module Marker
    # Marker value type
    class ValueType < Types::BaseObject
      graphql_name 'MarkerValueType'

      global_id_field :id
      field :created_at, GraphQL::Types::ISO8601DateTime, null: true
      field :updated_at, GraphQL::Types::ISO8601DateTime, null: true
      field :layer_field, Types::Layer::FieldType, null: true

      field :string_value, String, null: true
      def string_value
        object.string_value || object.string_value_translations.first[1]
      end

      field :text_value, String, null: true
      def text_value
        object.text_value || object.text_value_translations.first[1]
      end

      field :integer_value, Int, null: true
      field :float_value, Float, null: true
      field :boolean_value, Boolean, null: true
      field :datetime_value, GraphQL::Types::ISO8601DateTime, null: true
      field :date_value, GraphQL::Types::ISO8601Date, null: true
      field :time_value, GraphQL::Types::ISO8601DateTime, null: true
      field :layer_field_option, Types::Layer::FieldOptionType, null: true
    end
  end
end
