# frozen_string_literal: true

module Types
  # GraphQL info type
  class InfoType < Types::BaseObject
    field :server_version, String, null: true
    def server_version
      Bululu::VERSION
    end

    field :server_date, GraphQL::Types::ISO8601DateTime, null: true
    def server_date
      Time.current
    end

    field :scopes, [String], null: true
    def scopes
      context.current_client_app.scopes
    end
  end
end
