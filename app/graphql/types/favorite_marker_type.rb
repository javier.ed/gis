# frozen_string_literal: true

module Types
  # Favorite marker type
  class FavoriteMarkerType < Types::BaseObject
    global_id_field :id
    field :marker, Types::MarkerType, null: true
  end
end
