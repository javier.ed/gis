# frozen_string_literal: true

module Types
  # Country type
  class CountryType < Types::BaseObject
    global_id_field :id
    def id
      object.alpha2
    end

    field :name, String, null: true
  end
end
