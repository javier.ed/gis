# frozen_string_literal: true

module Types
  # GraphQL object type class for errors in mutations
  class ErrorType < GraphQL::Schema::Object
    description 'An user-readable error'

    field :message, String, null: false, description: 'A description of the error'
    field :path, [String], null: true, description: 'Which input value this error came from'
  end
end
