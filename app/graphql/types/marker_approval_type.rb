# frozen_string_literal: true

module Types
  # Marker approval type
  class MarkerApprovalType < Types::BaseObject
    global_id_field :id

    field :user, Types::UserType, null: true
    field :marker, Types::MarkerType, null: true
    field :approved, Boolean, null: true
    field :comment, String, null: true
  end
end
