# frozen_string_literal: true

module Types
  # User type
  class UserType < Types::BaseObject
    global_id_field :id
    field :created_at, GraphQL::Types::ISO8601DateTime, null: true
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: true
    field :username, String, null: true
    field :profile, Types::User::ProfileType, null: true

    field :email, String, null: true do
      def authorized?(object, args, context)
        super && object.id == context.current_user&.id
      end
    end

    field :email_verified, Boolean, null: true do
      def authorized?(object, args, context)
        super && object.id == context.current_user&.id
      end
    end
    def email_verified
      object.email_verified_at.present?
    end

    field :layers, Types::LayerType.connection_type, null: true do
      argument :query, String, required: false
      argument :order_by, String, required: false, default_value: 'id'
      argument :order_direction, String, required: false, default_value: 'desc'
    end
    def layers(query: nil, order_by: :id, order_direction: :desc)
      object.layers.search(query, order: { order_by => order_direction })
    end

    field :layer, Types::LayerType, null: true do
      argument :id, ID, required: true
    end
    def layer(id:)
      object.layers.find(id)
    rescue ActiveRecord::RecordNotFound => e
      Rails.logger.error e.message
      Rails.logger.error e.backtrace.join("\n")
      nil
    end

    field :markers, Types::MarkerType.connection_type, null: true do
      argument :query, String, required: false
      argument :order_by, String, required: false, default_value: 'id'
      argument :order_direction, String, required: false, default_value: 'desc'
    end
    def markers(query: nil, order_by: :id, order_direction: :desc)
      object.markers.search(
        query, order: { order_by => order_direction }, include_unpublished: (context.current_user == object)
      )
    end

    field :marker, Types::MarkerType, null: true do
      argument :id, ID, required: true
    end
    def marker(id:)
      object.markers.find(id)
    rescue ActiveRecord::RecordNotFound => e
      Rails.logger.error e.message
      Rails.logger.error e.backtrace.join("\n")
      nil
    end

    field :layer_subscriptions, type: Types::Layer::SubscriptionType.connection_type, null: true
  end
end
