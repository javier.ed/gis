# frozen_string_literal: true

module Types
  # Geocoding point type
  class GeocodingPointType < Types::BaseObject
    global_id_field :id
    field :created_at, GraphQL::Types::ISO8601DateTime, null: true
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: true

    field :address, String, null: true
    def address
      object.address || object.address_translations.first[1]
    end

    field :latitude, Float, null: true
    field :longitude, Float, null: true
  end
end
