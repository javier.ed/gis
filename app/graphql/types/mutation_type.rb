# frozen_string_literal: true

module Types
  # Mutation type
  class MutationType < Types::BaseObject
    description 'Mutations'

    field :change_email, mutation: Mutations::ChangeEmailMutation
    field :change_password, mutation: Mutations::ChangePasswordMutation
    field :clone_marker, mutation: Mutations::CloneMarkerMutation
    field :login, mutation: Mutations::LoginMutation
    field :logout, mutation: Mutations::LogoutMutation
    field :register, mutation: Mutations::RegisterMutation
    field :request_password_reset, mutation: Mutations::RequestPasswordResetMutation
    field :resend_email_verification, mutation: Mutations::ResendEmailVerificationMutation
    field :reset_password, mutation: Mutations::ResetPasswordMutation
    field :update_profile, mutation: Mutations::UpdateProfileMutation
    field :verify_email, mutation: Mutations::VerifyEmailMutation

    field :create_layer, mutation: Mutations::CreateLayerMutation
    field :update_layer, mutation: Mutations::UpdateLayerMutation
    field :delete_layer, mutation: Mutations::DeleteLayerMutation

    field :create_marker, mutation: Mutations::CreateMarkerMutation
    field :update_marker, mutation: Mutations::UpdateMarkerMutation
    field :delete_marker, mutation: Mutations::DeleteMarkerMutation

    field :create_marker_approval, mutation: Mutations::CreateMarkerApprovalMutation

    field :add_marker_to_favorites, mutation: Mutations::AddMarkerToFavoritesMutation
    field :remove_marker_from_favorites, mutation: Mutations::RemoveMarkerFromFavoritesMutation

    field :subscribe_to_layer, mutation: Mutations::SubscribeToLayerMutation
    field :unsubscribe_from_layer, mutation: Mutations::UnsubscribeFromLayerMutation
  end
end
