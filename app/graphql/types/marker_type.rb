# frozen_string_literal: true

require 'bululu/routable'

module Types
  # Marker type
  class MarkerType < Types::BaseObject
    include Bululu::Routable

    global_id_field :id
    field :created_at, GraphQL::Types::ISO8601DateTime, null: true
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: true

    field :name, String, null: true
    def name
      object.name || object.name_translations.first[1]
    end

    field :description, String, null: true
    def description
      object.description || object.description_translations.first[1]
    end

    field :start_at, GraphQL::Types::ISO8601DateTime, null: true
    field :finish_at, GraphQL::Types::ISO8601DateTime, null: true
    field :layer, Types::LayerType, null: true
    field :user, Types::UserType, null: true
    field :geocoding_point, Types::GeocodingPointType, null: true
    field :values, Types::Marker::ValueType.connection_type, null: true

    field :avatar_url, String, null: true do
      argument :resize_to_limit, [Int], required: false
    end
    def avatar_url(resize_to_limit: nil)
      return nil unless object.avatar.attached?

      return url_for(object.avatar.variant(resize_to_limit: resize_to_limit)) if resize_to_limit&.size == 2

      url_for(object.avatar)
    end

    field :published_at, GraphQL::Types::ISO8601DateTime, null: true

    field :published, Boolean, null: true
    def published
      object.published_at.present?
    end

    field :approval, Types::MarkerApprovalType, null: true
    def approval
      object.marker_approval
    end

    field :current_user_favorite, Types::FavoriteMarkerType, null: true
    def current_user_favorite
      return nil if context.current_user.blank?

      object.favorite_markers.find_by(user: context.current_user)
    end
  end
end
