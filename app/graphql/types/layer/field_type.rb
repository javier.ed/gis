# frozen_string_literal: true

module Types
  module Layer
    # Layer field type
    class FieldType < Types::BaseObject
      graphql_name 'LayerFieldType'

      global_id_field :id
      field :field_type, String, null: true

      field :name, String, null: true
      def name
        object.name || object.name_translations.first[1]
      end

      field :description, String, null: true
      def description
        object.description || object.description_translations.first[1]
      end

      field :required, Boolean, null: true
      field :unique, Boolean, null: true
      field :editable, Boolean, null: true
      field :options, Types::Layer::FieldOptionType.connection_type, null: true
    end
  end
end
