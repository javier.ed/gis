# frozen_string_literal: true

module Types
  module Layer
    # Layer field option type
    class FieldOptionType < Types::BaseObject
      graphql_name 'LayerFieldOptionType'

      global_id_field :id

      field :name, String, null: true
      def name
        object.name || object.name_translations.first[1]
      end
    end
  end
end
