# frozen_string_literal: true

module Types
  module Layer
    # Layer subscription type
    class SubscriptionType < Types::BaseObject
      global_id_field :id
      field :layer, Types::LayerType, null: true
    end
  end
end
