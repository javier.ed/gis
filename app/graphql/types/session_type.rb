# frozen_string_literal: true

module Types
  # Session type
  class SessionType < Types::BaseObject
    global_id_field :id
    field :created_at, GraphQL::Types::ISO8601DateTime, null: true
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: true
    field :key, String, null: true
  end
end
