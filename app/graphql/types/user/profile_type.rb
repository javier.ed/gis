# frozen_string_literal: true

require 'bululu/routable'

module Types
  module User
    # User profile type
    class ProfileType < Types::BaseObject
      include Bululu::Routable

      graphql_name 'UserProfileType'

      global_id_field :id
      field :created_at, GraphQL::Types::ISO8601DateTime, null: true
      field :updated_at, GraphQL::Types::ISO8601DateTime, null: true
      field :name, String, null: true
      field :bio, String, null: true
      field :country_id, String, null: true
      field :birthdate, GraphQL::Types::ISO8601Date, null: true
      field :country, Types::CountryType, null: true

      field :avatar_url, String, null: true do
        argument :resize_to_limit, [Int], required: false
      end
      def avatar_url(resize_to_limit: nil)
        return nil unless object.avatar.attached?

        return url_for(object.avatar.variant(resize_to_limit: resize_to_limit)) if resize_to_limit&.size == 2

        url_for(object.avatar)
      end
    end
  end
end
