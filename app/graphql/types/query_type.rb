# frozen_string_literal: true

module Types
  # Query type
  class QueryType < Types::BaseObject
    description 'Queries'

    field :info, Types::InfoType, null: true
    def info
      {}
    end

    field :current_user, Types::UserType, null: true
    delegate :current_user, to: :context

    field :user, Types::UserType, null: true do
      argument :username, String, required: true
    end
    def user(username:)
      ::User.find_by_username(username)
    end

    field :layer, Types::LayerType, null: true do
      argument :id, ID, required: true
    end
    def layer(id:)
      ::Layer.find(id)
    rescue ActiveRecord::RecordNotFound => e
      Rails.logger.error e.message
      Rails.logger.error e.backtrace.join("\n")
      nil
    end

    field :layers, Types::LayerType.connection_type, null: true do
      argument :query, String, required: false
      argument :order_by, String, required: false, default_value: 'id'
      argument :order_direction, String, required: false, default_value: 'desc'
    end
    def layers(query: nil, order_by: :id, order_direction: :desc)
      ::Layer.search(query, order: { order_by => order_direction })
    end

    field :geocoding_point, Types::GeocodingPointType, null: true do
      argument :id, ID, required: true
    end
    def geocoding_point(id:)
      GeocodingPoint.find(id)
    rescue ActiveRecord::RecordNotFound => e
      Rails.logger.error e.message
      Rails.logger.error e.backtrace.join("\n")
      nil
    end

    field :search_geocoding_points, Types::GeocodingPointType.connection_type, null: true do
      argument :query, String, required: false
      argument :order_by, String, required: false, default_value: 'id'
      argument :order_direction, String, required: false, default_value: 'desc'
    end
    def search_geocoding_points(query: nil, order_by: :id, order_direction: :desc)
      GeocodingPoint.search(query, order: { order_by => order_direction })
    end

    field :locate_geocoding_points, Types::GeocodingPointType.connection_type, null: true do
      argument :latitude, Float, required: true
      argument :longitude, Float, required: true
      argument :area, Float, required: false, default_value: 20
      argument :order_by, String, required: false, default_value: 'id'
      argument :order_direction, String, required: false, default_value: 'desc'
    end
    def locate_geocoding_points(latitude:, longitude:, area: 20, order_by: :id, order_direction: :desc)
      GeocodingPoint.locate(latitude, longitude, area: area, order: { order_by => order_direction })
    end

    field :reverse_geocode, Types::GeocodingPointType, null: true do
      argument :latitude, Float, required: true
      argument :longitude, Float, required: true
    end
    def reverse_geocode(latitude:, longitude:)
      Geocoder.search([latitude, longitude]).first
    end

    field :marker, Types::MarkerType, null: true do
      argument :id, ID, required: true
    end
    def marker(id:)
      ::Marker.find(id)
    rescue ActiveRecord::RecordNotFound => e
      Rails.logger.error e.message
      Rails.logger.error e.backtrace.join("\n")
      nil
    end

    field :search_markers, Types::MarkerType.connection_type, null: true do
      argument :query, String, required: false
      argument :layer_id, ID, required: false
      argument :order_by, String, required: false, default_value: 'id'
      argument :order_direction, String, required: false, default_value: 'desc'
    end
    def search_markers(query: nil, layer_id: nil, order_by: :id, order_direction: :desc)
      ::Marker.search(query, layer_id: layer_id, order: { order_by => order_direction })
    end

    field :locate_markers, Types::MarkerType.connection_type, null: true do
      argument :latitude, Float, required: true
      argument :longitude, Float, required: true
      argument :area, Float, required: false, default_value: 20
      argument :layer_id, ID, required: false
      argument :date_and_time, GraphQL::Types::ISO8601DateTime, required: false
      argument :order_by, String, required: false, default_value: 'id'
      argument :order_direction, String, required: false, default_value: 'desc'
    end
    def locate_markers( # rubocop:disable Metrics/ParameterLists
      latitude:, longitude:, area: 20, layer_id: nil, date_and_time: nil, order_by: :id, order_direction: :desc
    )
      ::Marker.locate(
        latitude, longitude, area: area, layer_id: layer_id, date_and_time: date_and_time,
                             order: { order_by => order_direction }
      )
    end

    field :favorite_markers, Types::FavoriteMarkerType.connection_type, null: true
    def favorite_markers
      current_user&.favorite_markers&.joins(:marker)&.where('markers.published_at IS NOT NULL')
    end

    field :locate_favorite_markers, Types::FavoriteMarkerType.connection_type, null: true do
      argument :latitude, Float, required: true
      argument :longitude, Float, required: true
      argument :area, Float, required: false, default_value: 20
      argument :order_by, String, required: false, default_value: 'id'
      argument :order_direction, String, required: false, default_value: 'desc'
    end
    def locate_favorite_markers(latitude:, longitude:, area: 20, order_by: :id, order_direction: :desc)
      current_user&.favorite_markers&.locate(latitude, longitude, area: area, order: { order_by => order_direction })
    end

    field :layer_subscriptions, Types::Layer::SubscriptionType.connection_type, null: true
    def layer_subscriptions
      current_user&.layer_subscriptions || LayerSubscription.where('user_id IS NULL')
    end

    field :all_countries, [Types::CountryType], null: true
    def all_countries
      ISO3166::Country.all.sort_by(&:name)
    end
  end
end
