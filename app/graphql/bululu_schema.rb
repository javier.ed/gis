# frozen_string_literal: true

# GraphQL schema
class BululuSchema < GraphQL::Schema
  mutation Types::MutationType
  query Types::QueryType

  context_class Contexts::CustomContext

  # Opt in to the new runtime (default in future graphql-ruby versions)
  use GraphQL::Execution::Interpreter
  use GraphQL::Analysis::AST

  # Add built-in connections for pagination
  use GraphQL::Pagination::Connections

  # Always wrap backtraces with GraphQL annotation
  use GraphQL::Backtrace

  def self.id_from_object(object, _type, _ctx)
    object.id
  end
end
