# frozen_string_literal: true

module Inputs
  # GraphQL input for layer mutations
  class LayerInput < Inputs::BaseInput
    description 'Input for layer mutations'

    argument :name, String, required: true
    argument :description, String, required: true
    argument :time_interval, String, 'Allowed values are `any`, `disabled` and any natural language date like `1 day`',
             required: true
    argument :start_at_current_time, Boolean, 'Only available if `timeInterval` is not disabled', required: false
    argument :suggestions, String, 'Allowed values are `requires_approval`, `publish_directly` and `deny`',
             required: true
    argument :fields, [Inputs::Layer::FieldInput], required: false
    argument :locale, String, required: false, default_value: I18n.default_locale
    argument :translate, Boolean, 'Only available on create', required: false, default_value: false
  end
end
