# frozen_string_literal: true

module Inputs
  module Marker
    # GraphQL input for marker value mutations
    class ValueInput < Inputs::BaseInput
      graphql_name 'MarkerValueInput'

      description 'Input for marker value mutations'

      argument :layer_field_id, ID, required: true
      argument :string_value, String, required: false
      argument :text_value, String, required: false
      argument :integer_value, Int, required: false
      argument :float_value, Float, required: false
      argument :boolean_value, Boolean, required: false
      argument :datetime_value, GraphQL::Types::ISO8601DateTime, required: false
      argument :date_value, GraphQL::Types::ISO8601Date, required: false
      argument :time_value, GraphQL::Types::ISO8601DateTime, required: false
      argument :layer_field_option_id, ID, required: false
    end
  end
end
