# frozen_string_literal: true

module Inputs
  # Geocoding point input
  class GeocodingPointInput < Inputs::BaseInput
    argument :latitude, Float, required: true
    argument :longitude, Float, required: true
  end
end
