# frozen_string_literal: true

module Inputs
  # User input
  class UserInput < Inputs::BaseInput
    argument :username, String, required: true
    argument :email, String, required: true
    argument :password, String, required: true
    argument :password_confirmation, String, required: false
  end
end
