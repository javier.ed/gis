# frozen_string_literal: true

module Inputs
  # Password input
  class PasswordInput < Inputs::BaseInput
    argument :password, String, required: true
    argument :password_confirmation, String, required: false
  end
end
