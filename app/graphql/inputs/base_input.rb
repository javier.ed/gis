# frozen_string_literal: true

module Inputs
  # Base input
  class BaseInput < GraphQL::Schema::InputObject
    argument_class Types::BaseArgument
  end
end
