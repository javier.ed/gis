# frozen_string_literal: true

module Inputs
  # GraphQL input for marker mutations
  class MarkerInput < Inputs::BaseInput
    description 'Input for marker mutations'

    argument :layer_id, ID, 'Required only to create the marker', required: false
    argument :geocoding_point, Inputs::GeocodingPointInput, required: true
    argument :name, String, required: true
    argument :description, String, required: false
    argument :start_at, GraphQL::Types::ISO8601DateTime, required: false
    argument :finish_at, GraphQL::Types::ISO8601DateTime, required: false
    argument :avatar, ApolloUploadServer::Upload, required: false
    argument :remove_avatar, Boolean, required: false
    argument :values, [Inputs::Marker::ValueInput], required: false
    argument :locale, String, required: false, default_value: I18n.default_locale
    argument :translate, Boolean, 'Only available on create', required: false, default_value: false
  end
end
