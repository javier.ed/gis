# frozen_string_literal: true

module Inputs
  # Marker approval input
  class MarkerApprovalInput < Inputs::BaseInput
    argument :marker_id, ID, required: true
    argument :approved, Boolean, required: false
    argument :comment, String, required: false
  end
end
