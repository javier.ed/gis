# frozen_string_literal: true

module Inputs
  module Layer
    # Layer field input
    class FieldInput < Inputs::BaseInput
      graphql_name 'LayerFieldInput'

      argument :id, ID, 'Only used when is updating or deleting the field', required: false
      argument :field_type, String, 'Only used when is creating the field', required: false
      argument :name, String, required: true
      argument :description, String, required: false
      argument :required, Boolean, required: false
      argument :unique, Boolean, required: false
      argument :editable, Boolean, required: false, default_value: true
      argument :options, [String], 'Only used when is creating the field', required: false
      argument :delete, Boolean, 'Only used when is deleting the field', required: false
    end
  end
end
