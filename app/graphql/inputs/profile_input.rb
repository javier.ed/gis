# frozen_string_literal: true

module Inputs
  # GraphQL input for profile mutations
  class ProfileInput < Inputs::BaseInput
    description 'Input for profile mutations'

    argument :name, String, 'Display name', required: false
    argument :bio, String, 'Bio', required: false
    argument :country_id, String, 'Country code (use only ISO 3166-1 alpha2 codes)', required: false
    argument :birthdate, GraphQL::Types::ISO8601Date, 'Birthdate', required: false
    argument :avatar, ApolloUploadServer::Upload, 'To replace current avatar image', required: false
    argument :remove_avatar, Boolean, 'To remove current avatar image', required: false
  end
end
