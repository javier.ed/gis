# frozen_string_literal: true

require 'bululu/graphql'

module Mutations
  # GraphQL mutation to create layers
  class CreateLayerMutation < Mutations::BaseMutation
    include Bululu::Graphql::AuthenticateUser
    include Bululu::Graphql::Scopes

    description 'Mutation to create layers'

    required_scopes 'layers.write'

    argument :attributes, Inputs::LayerInput, required: true

    field :layer, Types::LayerType, null: true

    def resolve(attributes:)
      QueuesService.call(action: :create_layer, scope: "user_id_#{current_user.id}") do
        resource = current_user.layers.new(
          name: attributes.name.strip,
          description: attributes.description.strip,
          time_interval: attributes.time_interval,
          start_at_current_time: attributes.start_at_current_time,
          suggestions: attributes.suggestions,
          locale: attributes.locale,
          translate: attributes.translate
        )

        assign_fields(resource, attributes)

        if resource.save
          { success: true, message: I18n.t('mutations.create_layer_mutation.success'), layer: resource }
        else
          error_response parse_errors(resource, %w[attributes])
        end
      rescue StandardError => e
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.join("\n")
        error_response
      end
    end

    private

    def assign_fields(resource, attributes)
      resource.fields = attributes.fields&.map do |field|
        LayerField.new(
          field_type: field.field_type.strip,
          name: field.name.strip,
          description: field.description&.strip,
          required: field.required,
          unique: field.unique,
          editable: field.editable,
          options: field.options&.map do |option|
            LayerFieldOption.new(name: option.strip, locale: attributes.locale, translate: attributes.translate)
          end || [],
          locale: attributes.locale,
          translate: attributes.translate
        )
      end || []
    end

    def error_response(errors = nil)
      { success: false, message: I18n.t('mutations.create_layer_mutation.fail'), errors: errors }
    end
  end
end
