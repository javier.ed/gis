# frozen_string_literal: true

require 'bululu/graphql'

module Mutations
  # GraphQL mutation to update layers
  class UpdateLayerMutation < Mutations::BaseMutation
    include Bululu::Graphql::AuthenticateUser
    include Bululu::Graphql::Scopes

    description 'Mutation to update layers'

    required_scopes 'layers.write'

    argument :id, ID, required: true
    argument :attributes, Inputs::LayerInput, required: true

    field :layer, Types::LayerType, null: true

    def resolve(id:, attributes:)
      QueuesService.call(action: :update_layer, scope: "user_id_#{current_user.id}") do
        resource = current_user.layers.find(id)

        attributes.fields&.each do |field|
          if field.id.present?
            update_field(resource, field, attributes.locale)
          else
            build_field(resource, field, attributes.locale)
          end
        end

        if resource.update(
          name: attributes.name.strip,
          description: attributes.description.strip,
          time_interval: attributes.time_interval,
          start_at_current_time: attributes.start_at_current_time,
          suggestions: attributes.suggestions,
          locale: attributes.locale
        )
          { success: true, message: I18n.t('mutations.update_layer_mutation.success'), layer: resource }
        else
          error_response(parse_errors(resource, %w[attributes]))
        end
      rescue StandardError => e
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.join("\n")
        error_response
      end
    end

    private

    def update_field(resource, field, locale)
      resource_field = resource.fields.filter { |f| f.id.to_s == field.id.to_s }.first
      if field.delete
        resource_field.mark_for_destruction
      else
        resource_field.assign_attributes(
          name: field.name.strip,
          description: field.description&.strip,
          required: field.required,
          unique: field.unique,
          editable: field.editable,
          locale: locale
        )
      end
    end

    def build_field(resource, field, locale)
      resource.fields.build(
        field_type: field.field_type.strip,
        name: field.name.strip,
        description: field.description&.strip,
        required: field.required,
        unique: field.unique,
        editable: field.editable,
        options: field.options&.map { |option| LayerFieldOption.new(name: option.strip, locale: locale) } || [],
        locale: locale
      )
    end

    def error_response(errors = nil)
      { success: false, message: I18n.t('mutations.update_layer_mutation.fail'), errors: errors }
    end
  end
end
