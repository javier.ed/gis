# frozen_string_literal: true

require 'bululu/graphql'

module Mutations
  # Subscribe to layer mutation
  class SubscribeToLayerMutation < Mutations::BaseMutation
    include Bululu::Graphql::AuthenticateUser

    argument :layer_id, ID, required: true

    field :layer_subscription, Types::Layer::SubscriptionType, null: true

    def resolve(layer_id:)
      QueuesService.call(action: :unsubscribe_from_layer, scope: "user_id_#{current_user.id}") do
        resource = current_user.layer_subscriptions.new(layer_id: layer_id)

        if resource.save
          {
            success: true, message: I18n.t('mutations.subscribe_to_layer_mutation.success'),
            layer_subscription: resource
          }
        else
          error_response(parse_errors(resource))
        end
      rescue StandardError => e
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.join("\n")
        error_responses
      end
    end

    private

    def error_response(errors = nil)
      { success: false, message: I18n.t('mutations.subscribe_to_layer_mutation.fail'), errors: errors }
    end
  end
end
