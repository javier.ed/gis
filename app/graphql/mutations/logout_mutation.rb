# frozen_string_literal: true

require 'bululu/graphql'

module Mutations
  # Logout mutation
  class LogoutMutation < Mutations::BaseMutation
    include Bululu::Graphql::AuthenticateUser
    include Bululu::Graphql::Scopes

    required_scopes 'users.authenticate'

    def resolve
      QueuesService.call(action: :logout, scope: "user_id_#{current_user.id}") do
        if current_session.update finished_at: Time.current
          { success: true, message: I18n.t('mutations.logout_mutation.success') }
        else
          { success: false, message: I18n.t('mutations.logout_mutation.fail') }
        end
      end
    end
  end
end
