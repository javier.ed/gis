# frozen_string_literal: true

require 'bululu/graphql'

module Mutations
  # Remove marker from favorites mutation
  class RemoveMarkerFromFavoritesMutation < Mutations::BaseMutation
    include Bululu::Graphql::AuthenticateUser

    argument :id, ID, required: true

    def resolve(id:)
      QueuesService.call(action: :remove_marker_from_favorites, scope: "user_id_#{current_user.id}") do
        resource = current_user.favorite_markers.find(id)

        if resource.destroy
          { success: true, message: I18n.t('mutations.remove_marker_from_favorites_mutation.success') }
        else
          error_response
        end
      rescue StandardError => e
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.join("\n")
        error_response
      end
    end

    private

    def error_response
      { success: false, message: I18n.t('mutations.remove_marker_from_favorites_mutation.fail') }
    end
  end
end
