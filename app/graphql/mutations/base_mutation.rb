# frozen_string_literal: true

module Mutations
  # Base mutation
  class BaseMutation < GraphQL::Schema::Mutation
    argument_class Types::BaseArgument
    field_class Types::BaseField
    object_class Types::BaseObject

    field :success, Boolean, null: true
    field :message, String, null: true
    field :errors, [Types::ErrorType], null: true

    delegate :current_client_app, :current_session, :current_user, :remote_ip, to: :context

    # @param resource [Object]
    # @param base_path [Array<String, Symbol>]
    # @param level [Integer]
    # @return [Array<Hash>]
    def parse_errors(resource, base_path = [], level = 0)
      base_path = base_path.collect { |k| k.to_s.camelize(:lower) }

      errors = resource.errors.map do |attribute, message|
        path = base_path + [attribute.to_s.camelize(:lower)]
        { path: path, message: message }
      end

      if level < 3
        resource.nested_attributes_options.each do |key, _|
          res = resource.send(key)
          path = base_path + [key.to_s.camelize(:lower)]

          if res.is_a?(ActiveRecord::Associations::CollectionProxy)
            i = 0
            res.each do |r|
              errors += parse_errors(r, path + [i], level + 1)
              i += 1
            end
          elsif res
            errors += parse_errors(res, path, level + 1)
          end
        end
      end

      errors
    end
  end
end
