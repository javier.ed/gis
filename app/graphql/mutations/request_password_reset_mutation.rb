# frozen_string_literal: true

require 'bululu/graphql'

module Mutations
  # Request password reset mutation
  class RequestPasswordResetMutation < Mutations::BaseMutation
    include Bululu::Graphql::RequireNoAuthentication
    include Bululu::Graphql::Scopes

    required_scopes 'users.authenticate'

    argument :login, String, required: true

    def resolve(login:)
      QueuesService.call(action: :request_password_reset, scope: "client_app_id_#{current_client_app.id}") do
        user = User.find_by_login login

        return error_response unless user

        resource = user.otps.build action: 'reset_password'

        if resource.save
          { success: true, message: I18n.t('mutations.request_password_reset_mutation.success') }
        else
          error_response
        end
      end
    end

    private

    def error_response
      { success: false, message: I18n.t('mutations.request_password_reset_mutation.fail') }
    end
  end
end
