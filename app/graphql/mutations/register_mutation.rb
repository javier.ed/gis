# frozen_string_literal: true

require 'bululu/fail2ban'
require 'bululu/graphql'

module Mutations
  # Register mutation
  class RegisterMutation < Mutations::BaseMutation
    include Bululu::Graphql::RequireNoAuthentication
    include Bululu::Graphql::Scopes
    include Bululu::Fail2ban

    required_scopes 'users.create'

    description 'Attempt to register an user'

    argument :attributes, Inputs::UserInput, 'User attributes', required: true

    field :user, Types::UserType, 'User', null: true
    field :session, Types::SessionType, 'User', null: true

    def resolve(attributes:)
      QueuesService.call(action: :register, scope: "client_app_id_#{current_client_app.id}") do
        resource = User.new(
          username: attributes.username, email: attributes.email, password: attributes.password,
          password_confirmation: attributes.password_confirmation
        )

        if resource.save
          session = resource.sessions.create!(client_app: current_client_app)
          { success: true, message: I18n.t('mutations.register_mutation.success'), user: resource, session: session }
        else
          add_to_fail2ban(remote_ip, 'FAILED TO REGISTER USER')
          {
            success: false,
            message: I18n.t('mutations.register_mutation.fail'),
            errors: parse_errors(resource, %w[attributes])
          }
        end
      end
    end
  end
end
