# frozen_string_literal: true

require 'bululu/graphql'

module Mutations
  # GraphQL mutation to clone markers
  class CloneMarkerMutation < Mutations::BaseMutation
    include Bululu::Graphql::AuthenticateUser
    include Bululu::Graphql::Scopes

    description 'Mutation to clone markers'

    required_scopes 'markers.write'

    argument :id, ID, required: true

    field :marker, Types::MarkerType, null: true

    def resolve(id:)
      QueuesService.call(action: :duplicate_marker, scope: "user_id_#{current_user.id}") do
        marker = Marker.find(id)

        return error_response if current_user != marker.layer.user && marker.layer.suggestions == 'deny'

        resource = marker.build_duplicate(user: current_user)

        if resource.save
          { success: true, message: I18n.t('mutations.clone_marker_mutation.success'), marker: resource }
        else
          error_response(parse_errors(resource, %w[attributes]))
        end
      rescue StandardError => e
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.join("\n")
        error_response
      end
    end

    private

    def error_response(errors = nil)
      { success: false, message: I18n.t('mutations.clone_marker_mutation.fail'), errors: errors }
    end
  end
end
