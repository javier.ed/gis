# frozen_string_literal: true

require 'bululu/graphql'

module Mutations
  # GraphQL mutation to delete layers
  class DeleteLayerMutation < Mutations::BaseMutation
    include Bululu::Graphql::AuthenticateUser
    include Bululu::Graphql::Scopes

    description 'Mutation to delete layers'

    required_scopes 'layers.write'

    argument :id, ID, required: true

    def resolve(id:)
      QueuesService.call(action: :delete_layer, scope: "user_id_#{current_user.id}") do
        resource = current_user.layers.find(id)

        if resource.destroy
          { success: true, message: I18n.t('mutations.delete_layer_mutation.success') }
        else
          error_response
        end
      rescue StandardError => e
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.join("\n")
        error_response
      end
    end

    private

    def error_response
      { success: false, message: I18n.t('mutations.delete_layer_mutation.fail') }
    end
  end
end
