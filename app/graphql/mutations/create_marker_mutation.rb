# frozen_string_literal: true

module Mutations
  # GraphQL mutation to create markers
  class CreateMarkerMutation < Mutations::BaseMarkerMutation
    description 'Mutation to create markers'

    def resolve(attributes:)
      QueuesService.call(action: :create_marker, scope: "user_id_#{current_user.id}") do
        layer = Layer.find(attributes.layer_id)

        return error_response if current_user != layer.user && layer.suggestions == 'deny'

        resource = layer.markers.new(
          user: current_user,
          name: attributes.name.strip,
          description: attributes.description&.strip,
          locale: attributes.locale,
          translate: attributes.translate
        )

        assign_geocoding_point(resource, attributes)
        assign_values(resource, attributes)
        assign_time_interval(resource, attributes)
        attach_avatar(resource, attributes)
        assign_published_at(resource)

        if resource.save
          { success: true, message: I18n.t('mutations.create_marker_mutation.success'), marker: resource }
        else
          error_response(parse_errors(resource, %w[attributes]))
        end
      rescue StandardError => e
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.join("\n")
        error_response
      end
    end

    private

    def assign_values(resource, attributes)
      resource.layer.fields&.map do |field|
        value = attributes.values&.filter { |v| v.layer_field_id.to_s == field.id.to_s }&.first
        resource.values.build(
          layer_field_id: field.id, **value_filtered(value, field.field_type), locale: attributes.locale
        )
      end
    end

    def error_response(errors = nil)
      { success: false, message: I18n.t('mutations.create_marker_mutation.fail'), errors: errors }
    end
  end
end
