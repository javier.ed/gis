# frozen_string_literal: true

require 'bululu/graphql'

module Mutations
  # Reset password mutation
  class ResetPasswordMutation < Mutations::BaseMutation
    include Bululu::Graphql::RequireNoAuthentication
    include Bululu::Graphql::Scopes

    required_scopes 'users.update'

    argument :login, String, required: true
    argument :confirmation_code, String, required: true
    argument :attributes, Inputs::PasswordInput, required: true

    def resolve(login:, confirmation_code:, attributes:)
      QueuesService.call(action: :reset_password, scope: "client_app_id_#{current_client_app.id}") do
        resource = User.find_by_login(login)

        return error_response unless resource

        otp = resource.otps.find_and_authenticate('reset_password', confirmation_code)

        return error_response([{ path: %w[confirmation_code], message: I18n.t('errors.messages.invalid') }]) unless otp

        if resource.change_password attributes.password, attributes.password_confirmation
          otp.mark_as_used
          { success: true, message: I18n.t('mutations.reset_password_mutation.success') }
        else
          error_response parse_errors(resource, %w[attributes])
        end
      end
    end

    private

    def error_response(errors = nil)
      { success: false, message: I18n.t('mutations.reset_password_mutation.fail'), errors: errors }
    end
  end
end
