# frozen_string_literal: true

require 'bululu/graphql'

module Mutations
  # Create marker approval mutation
  class CreateMarkerApprovalMutation < Mutations::BaseMutation
    include Bululu::Graphql::AuthenticateUser
    include Bululu::Graphql::Scopes

    required_scopes 'markers.write'

    argument :attributes, Inputs::MarkerApprovalInput, required: true

    field :marker_approval, Types::MarkerApprovalType, null: true

    def resolve(attributes:)
      QueuesService.call(action: :create_marker_approval, scope: "user_id_#{current_user.id}") do
        resource = current_user.marker_approvals.new(
          marker_id: attributes.marker_id, approved: attributes.approved, comment: attributes.comment.strip
        )

        if resource.save
          {
            success: true,
            message: I18n.t('mutations.create_marker_approval_mutation.success'),
            marker_approval: resource
          }
        else
          error_response(parse_errors(resource, %w[attributes]))
        end
      rescue StandardError => e
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.join("\n")
        error_response
      end
    end

    private

    def error_response(errors = nil)
      { success: false, message: I18n.t('mutations.create_marker_approval_mutation.fail'), errors: errors }
    end
  end
end
