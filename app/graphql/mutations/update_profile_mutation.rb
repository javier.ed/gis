# frozen_string_literal: true

require 'bululu/graphql'

module Mutations
  # GraphQL mutation to update the current user profile
  class UpdateProfileMutation < Mutations::BaseMutation
    include Bululu::Graphql::AuthenticateUser
    include Bululu::Graphql::Scopes

    description 'Mutation to update the current user profile'

    required_scopes 'users.update'

    argument :attributes, Inputs::ProfileInput, 'Attributes', required: true

    def resolve(attributes:)
      QueuesService.call(action: :update_profile, scope: "user_id_#{current_user.id}") do
        resource = Profile.find_or_initialize_by(user: current_user)
        resource.assign_attributes(
          name: attributes.name,
          bio: attributes.bio,
          country_id: attributes.country_id,
          birthdate: attributes.birthdate
        )

        update_avatar(resource, attributes)

        if resource.save
          { success: true, message: I18n.t('mutations.update_profile_mutation.success') }
        else
          error_response(parse_errors(resource, %w[attributes]))
        end
      rescue StandardError => e
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.join("\n")
        error_response
      end
    end

    private

    def error_response(errors = nil)
      { success: false, message: I18n.t('mutations.update_profile_mutation.fail'), errors: errors }
    end

    def update_avatar(resource, attributes)
      if resource.avatar.attached? && attributes.remove_avatar
        resource.avatar.detach
      elsif attributes.avatar.present?
        resource.avatar.attach(
          io: attributes.avatar,
          filename: attributes.avatar.original_filename,
          content_type: attributes.avatar.content_type
        )
      end
    end
  end
end
