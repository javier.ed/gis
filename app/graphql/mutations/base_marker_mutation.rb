# frozen_string_literal: true

require 'bululu/graphql'

module Mutations
  # GraphQL base mutation for markers
  class BaseMarkerMutation < Mutations::BaseMutation
    include Bululu::Graphql::AuthenticateUser
    include Bululu::Graphql::Scopes

    required_scopes 'markers.write'

    argument :attributes, Inputs::MarkerInput, required: true

    field :marker, Types::MarkerType, null: true

    def field_types_map(value)
      {
        'string' => { string_value: value.string_value },
        'text' => { text_value: value.text_value },
        'integer' => { integer_value: value.integer_value },
        'float' => { float_value: value.float_value },
        'boolean' => { boolean_value: value.boolean_value },
        'date' => { date_value: value.date_value },
        'time' => { time_value: value.time_value },
        'datetime' => { datetime_value: value.datetime_value },
        'choice' => { layer_field_option_id: value.layer_field_option_id }
      }
    end

    def value_filtered(value, field_type)
      return field_types_map(value)[field_type] if value.present?

      {}
    end

    def attach_avatar(resource, attributes)
      return if attributes.avatar.blank?

      resource.avatar.attach(
        io: attributes.avatar,
        filename: attributes.avatar.original_filename,
        content_type: attributes.avatar.content_type
      )
    end

    def assign_geocoding_point(resource, attributes)
      resource.geocoding_point = GeocodingPoint.find_or_initialize_by(
        latitude: attributes.geocoding_point.latitude,
        longitude: attributes.geocoding_point.longitude
      )
    end

    def assign_time_interval(resource, attributes)
      layer = resource.layer
      start_at = layer.start_at_current_time ? resource.start_at || Time.current : attributes.start_at

      if layer.time_interval == 'any'
        resource.assign_attributes(start_at: start_at, finish_at: attributes.finish_at)
      elsif layer.time_interval != 'disabled'
        resource.assign_attributes(
          start_at: start_at, finish_at: start_at + ChronicDuration.parse(layer.time_interval)
        )
      end
    end

    def assign_published_at(resource)
      layer = resource.layer
      resource.assign_attributes(
        published_at: (layer.user == current_user || layer.suggestions == 'publish_directly' ? Time.current : nil),
        marker_approval: nil
      )
    end
  end
end
