# frozen_string_literal: true

require 'bululu/graphql'

module Mutations
  # Add marker to favorites mutation
  class AddMarkerToFavoritesMutation < Mutations::BaseMutation
    include Bululu::Graphql::AuthenticateUser

    argument :marker_id, ID, required: true

    field :favorite_marker, Types::FavoriteMarkerType, null: true

    def resolve(marker_id:)
      QueuesService.call(action: :add_marker_to_favorites, scope: "user_id_#{current_user.id}") do
        resource = current_user.favorite_markers.new(marker_id: marker_id)

        if resource.save
          {
            success: true, message: I18n.t('mutations.add_marker_to_favorites_mutation.success'),
            favorite_marker: resource
          }
        else
          error_response(parse_errors(resource))
        end
      rescue StandardError => e
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.join("\n")
        error_responses
      end
    end

    private

    def error_response(errors = nil)
      { success: false, message: I18n.t('mutations.add_marker_to_favorites_mutation.fail'), errors: errors }
    end
  end
end
