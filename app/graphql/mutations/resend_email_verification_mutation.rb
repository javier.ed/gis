# frozen_string_literal: true

require 'bululu/graphql'

module Mutations
  # GraphQL mutation to resend current user email verification
  class ResendEmailVerificationMutation < Mutations::BaseMutation
    include Bululu::Graphql::AuthenticateUser

    description 'Mutation to resend current user email verification'

    def resolve
      QueuesService.call(action: :resend_email_verification, scope: "user_id_#{current_user.id}") do
        if current_user.resend_email_verification
          { success: true, message: I18n.t('mutations.resend_email_verification_mutation.success') }
        else
          error_response
        end
      rescue StandardError => e
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.join("\n")
        error_response
      end
    end

    private

    def error_response
      { success: false, message: I18n.t('mutations.resend_email_verification_mutation.fail') }
    end
  end
end
