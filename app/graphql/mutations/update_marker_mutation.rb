# frozen_string_literal: true

module Mutations
  # GraphQL mutation to update markers
  class UpdateMarkerMutation < Mutations::BaseMarkerMutation
    description 'Mutation to update markers'

    argument :id, ID, required: true

    def resolve(id:, attributes:)
      QueuesService.call(action: :update_marker, scope: "user_id_#{current_user.id}") do
        resource = current_user.markers.find(id)

        assign_geocoding_point(resource, attributes)
        update_values(resource, attributes.values, attributes.locale)
        assign_time_interval(resource, attributes)
        update_avatar(resource, attributes)
        assign_published_at(resource)

        if resource.update(
          name: attributes.name.strip,
          description: attributes.description&.strip,
          locale: attributes.locale
        )
          { success: true, message: I18n.t('mutations.update_marker_mutation.success'), marker: resource }
        else
          error_response(parse_errors(resource, %w[attributes]))
        end
      rescue StandardError => e
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.join("\n")
        error_response
      end
    end

    private

    def error_response(errors = nil)
      { success: false, message: I18n.t('mutations.update_marker_mutation.fail'), errors: errors }
    end

    def find_value_by_field_id(values, field)
      values&.filter { |v| v.layer_field_id.to_s == field.id.to_s }&.first
    end

    def update_values(resource, values, locale)
      resource.layer.fields&.map do |field|
        value = find_value_by_field_id(values, field)
        resource_value = find_value_by_field_id(resource.values, field)
        if resource_value.present?
          resource_value.assign_attributes(**value_filtered(value, field.field_type), locale: locale)
        else
          resource.values.build(layer_field_id: field.id, **value_filtered(value, field.field_type), locale: locale)
        end
      end
    end

    def update_avatar(resource, attributes)
      if resource.avatar.attached? && attributes.remove_avatar
        resource.avatar.detach
      else
        attach_avatar(resource, attributes)
      end
    end
  end
end
