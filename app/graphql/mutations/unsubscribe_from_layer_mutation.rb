# frozen_string_literal: true

require 'bululu/graphql'

module Mutations
  # Unsubscribe from layer mutation
  class UnsubscribeFromLayerMutation < Mutations::BaseMutation
    include Bululu::Graphql::AuthenticateUser

    argument :id, ID, required: true

    def resolve(id:)
      QueuesService.call(action: :unsubscribe_from_layer, scope: "user_id_#{current_user.id}") do
        resource = current_user.layer_subscriptions.find(id)

        if resource.destroy
          { success: true, message: I18n.t('mutations.unsubscribe_from_layer_mutation.success') }
        else
          error_response
        end
      rescue StandardError => e
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.join("\n")
        error_response
      end
    end

    private

    def error_response
      { success: false, message: I18n.t('mutations.unsubscribe_from_layer_mutation.fail') }
    end
  end
end
