# frozen_string_literal: true

require 'bululu/graphql'

module Mutations
  # Verify email mutation
  class VerifyEmailMutation < Mutations::BaseMutation
    include Bululu::Graphql::AuthenticateUser
    include Bululu::Graphql::Scopes

    required_scopes 'users.update'

    argument :confirmation_code, String, required: true

    def resolve(confirmation_code:)
      QueuesService.call(action: :update_user, scope: "id_#{current_user.id}") do
        if current_user.verify_email confirmation_code
          { success: true, message: I18n.t('mutations.verify_email_mutation.success') }
        else
          { success: false, message: I18n.t('mutations.verify_email_mutation.fail') }
        end
      end
    end
  end
end
