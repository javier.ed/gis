# frozen_string_literal: true

require 'bululu/graphql'
require 'bululu/fail2ban'

module Mutations
  # Login mutation
  class LoginMutation < Mutations::BaseMutation
    include Bululu::Graphql::RequireNoAuthentication
    include Bululu::Graphql::Scopes
    include Bululu::Fail2ban

    required_scopes 'users.authenticate'

    argument :login, String, required: true
    argument :password, String, required: true

    field :user, Types::UserType, null: true
    field :session, Types::SessionType, null: true

    def resolve(login:, password:)
      QueuesService.call(action: :login, scope: "client_app_id_#{current_client_app.id}") do
        user = User.find_by_login(login)

        return error_response unless user&.authenticate(password)

        resource = current_client_app.sessions.build(user: user)

        if resource.save
          { success: true, message: I18n.t('mutations.login_mutation.success'), user: user, session: resource }
        else
          error_response
        end
      end
    end

    private

    def error_response
      add_to_fail2ban(remote_ip, 'FAILED TO LOGIN')
      { success: false, message: I18n.t('mutations.login_mutation.fail') }
    end
  end
end
