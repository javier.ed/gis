# frozen_string_literal: true

require 'bululu/graphql'

module Mutations
  # GraphQL mutation to delete markers
  class DeleteMarkerMutation < Mutations::BaseMutation
    include Bululu::Graphql::AuthenticateUser
    include Bululu::Graphql::Scopes

    description 'Mutation to delete markers'

    required_scopes 'markers.write'

    argument :id, ID, required: true

    def resolve(id:)
      QueuesService.call(action: :delete_marker, scope: "user_id_#{current_user.id}") do
        resource = Marker.find(id)

        return error_response if resource.user != current_user && resource.layer.user != current_user

        if resource.destroy
          { success: true, message: I18n.t('mutations.delete_marker_mutation.success') }
        else
          error_response
        end
      rescue StandardError => e
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.join("\n")
        error_response
      end
    end

    private

    def error_response
      { success: false, message: I18n.t('mutations.delete_marker_mutation.fail') }
    end
  end
end
