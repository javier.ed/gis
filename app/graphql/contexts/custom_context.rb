# frozen_string_literal: true

module Contexts
  # Custom context
  class CustomContext < GraphQL::Query::Context
    # @param query [GraphQL::Query]
    # @param values [Hash]
    # @param object [Object]
    def initialize(query:, values:, object:)
      super

      self[:current_user_id] = authorization.current_user&.id if self[:channel]
    end

    # @return [Bululu::Authorization]
    def authorization
      self[:authorization]
    end

    # @return [Boolean]
    def introspection?
      self[:introspection]
    end

    # To get the current device
    #
    # @return [ClientApp, nil] Return the current device or nil
    def current_client_app # rubocop:disable Rails/Delegate
      authorization.current_client_app
    end

    # To get the current session
    #
    # @return [Session, nil] Return the current session or nil
    def current_session # rubocop:disable Rails/Delegate
      authorization.current_session
    end

    # To get the current user
    #
    # @return [User, nil] Return the current user or nil
    def current_user # rubocop:disable Rails/Delegate
      authorization.current_user
    end

    def current_scopes # rubocop:disable Rails/Delegate
      authorization.current_scopes
    end

    # @return [Boolean]
    def client_app_authorized? # rubocop:disable Rails/Delegate
      authorization.client_app_authorized?
    end

    # @return [Boolean]
    def user_authenticated? # rubocop:disable Rails/Delegate
      authorization.user_authenticated?
    end

    def remote_ip
      self[:remote_ip]
    end
  end
end
