# frozen_string_literal: true

# GraphQL Controller
class GraphqlController < ApplicationController
  # If accessing from outside this domain, nullify the session
  # This allows for outside API access while preventing CSRF attacks,
  # but you'll have to authenticate your user separately
  # protect_from_forgery with: :null_session

  before_action :require_client_app_authorization!

  def execute
    context = {
      authorization: authorization,
      backtrace: Rails.env.development?,
      introspection: (Rails.env.development? && params[:introspection]),
      remote_ip: request.remote_ip
    }

    result = if params[:_json]
               queries = params[:_json].map do |param|
                 {
                   query: param[:query],
                   operation_name: param[:operationName],
                   variables: ensure_hash(param[:variables]),
                   context: context
                 }
               end
               BululuSchema.multiplex(queries)
             else
               BululuSchema.execute(
                 params[:query],
                 variables: ensure_hash(params[:variables]),
                 context: context,
                 operation_name: params[:operationName]
               )
             end

    render json: result
  rescue StandardError => e
    raise e unless Rails.env.development?

    handle_error_in_development e
  end

  private

  # Handle form data, JSON body, or a blank value
  def ensure_hash(ambiguous_param)
    case ambiguous_param
    when String
      if ambiguous_param.present?
        ensure_hash(JSON.parse(ambiguous_param))
      else
        {}
      end
    when Hash, ActionController::Parameters
      ambiguous_param
    when nil
      {}
    else
      raise ArgumentError, "Unexpected parameter: #{ambiguous_param}"
    end
  end

  def handle_error_in_development(error)
    logger.error error.message
    logger.error error.backtrace.join("\n")

    render json: { error: { message: error.message, backtrace: error.backtrace }, data: {} }, status: 500
  end
end
