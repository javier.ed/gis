# frozen_string_literal: true

require 'bululu/authorization'

# Base controller
class ApplicationController < ActionController::API
  before_action :set_paper_trail_whodunnit

  # @return [nil]
  def require_client_app_authorization!
    return if authorization.client_app_authorized? || (Rails.env.development? && params[:introspection])

    render(json: { message: 'Unauthorized' }, status: :unauthorized)
  end

  # @return [nil]
  def authenticate_user!
    return if authorization.user_authenticated? || (Rails.env.development? && params[:introspection])

    render(json: { message: 'Unauthenticated' }, status: :unauthorized)
  end

  delegate :current_user, to: :authorization

  private

  # @return [Bululu::Authorization]
  def authorization
    return @authorization if @authorization

    @authorization = Bululu::Authorization.new request: request
  end
end
