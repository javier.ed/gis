# frozen_string_literal: true

# Home controller class
class HomeController < ApplicationController
  # @return [nil]
  def index
    render json: {
      name: I18n.t('app_name'),
      description: I18n.t('app_description'),
      version: Bululu::VERSION,
      environment: Rails.env
    }
  end
end
