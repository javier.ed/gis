# frozen_string_literal: true

# User mailer
class UserMailer < ApplicationMailer
  def welcome_mail
    @username = params[:username]

    mail to: params[:to]
  end
end
