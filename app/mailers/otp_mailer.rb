# frozen_string_literal: true

# OTP mailer
class OtpMailer < ApplicationMailer
  def password_mail
    @username = params[:username]
    @password = params[:password]
    @action = params[:action]

    mail to: params[:to]
  end
end
