# frozen_string_literal: true

# Base mailer
class ApplicationMailer < ActionMailer::Base
  default from: "#{I18n.t('app_name')} <#{Settings.mailer.sender_address}>"
  layout 'mailer'
end
