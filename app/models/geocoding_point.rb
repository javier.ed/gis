# frozen_string_literal: true

# Geocoding point model
class GeocodingPoint < ApplicationRecord
  has_paper_trail

  translates :address, versioning: :paper_trail

  attr_readonly :created_at

  has_many :markers, dependent: :restrict_with_exception

  reverse_geocoded_by :latitude, :longitude

  validates :latitude, :longitude, presence: true
  validates :latitude, numericality: { greater_than_or_equal_to: -90, less_than_or_equal_to: 90 }
  validates :longitude, numericality: { greater_than_or_equal_to: -180, less_than_or_equal_to: 180 }
  validates :latitude, uniqueness: { scope: :longitude }
  validates :address, length: { maximum: 256 }, allow_blank: true

  after_validation :reverse_geocode, :assign_coordinates

  # @param query [String]
  # @param order [Hash]
  # @return [ActiveRecord::Relation]
  def self.search(query, order: { id: :desc })
    near(query).order(order)
  rescue ArgumentError => e
    logger.error e.message
    logger.error e.backtrace.join("\n")
    nil
  end

  # @param latitude [Float]
  # @param longitude [Float]
  # @param area [Float]
  # @param order [Hash]
  # @return [ActiveRecord::Relation]
  def self.locate(latitude, longitude, area: 20, order: { id: :desc })
    near([latitude, longitude], area).order(order)
  rescue ArgumentError => e
    logger.error e.message
    logger.error e.backtrace.join("\n")
    nil
  end

  private

  def assign_coordinates
    self.coordinates = "POINT(#{longitude} #{latitude})"
  end
end
