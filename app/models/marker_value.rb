# frozen_string_literal: true

# Marker value model
class MarkerValue < ApplicationRecord
  has_paper_trail

  translates :string_value, :text_value, versioning: :paper_trail

  attr_readonly :created_at

  belongs_to :layer_field_option, optional: true
  belongs_to :layer_field, inverse_of: :marker_values
  belongs_to :marker, inverse_of: :values

  validates :marker_id, uniqueness: { scope: :layer_field_id }
  validates :string_value, presence: true, if: proc { layer_field&.field_type == 'string' && layer_field.required? }
  validates :text_value, presence: true, if: proc { layer_field&.field_type == 'text' && layer_field.required? }
  validates :integer_value, presence: true, if: proc { layer_field&.field_type == 'integer' && layer_field.required? }
  validates :float_value, presence: true, if: proc { layer_field&.field_type == 'float' && layer_field.required? }
  validates :date_value, presence: true, if: proc { layer_field&.field_type == 'date' && layer_field.required? }
  validates :time_value, presence: true, if: proc { layer_field&.field_type == 'time' && layer_field.required? }
  validates :datetime_value, presence: true, if: proc { layer_field&.field_type == 'datetime' && layer_field.required? }
  validates :layer_field_option_id, presence: true,
                                    if: proc { layer_field&.field_type == 'choice' && layer_field.required? }
  validates :string_value, uniqueness: {
    case_sensitive: false, scope: :layer_field_id
  }, if: proc { layer_field&.field_type == 'string' && layer_field.unique? }
  validates :text_value, uniqueness: {
    case_sensitive: false, scope: :layer_field_id
  }, if: proc { layer_field&.field_type == 'text' && layer_field.unique? }
  validates :integer_value, uniqueness: {
    case_sensitive: false, scope: :layer_field_id
  }, if: proc { layer_field&.field_type == 'integer' && layer_field.unique? }
  validates :float_value, uniqueness: {
    case_sensitive: false, scope: :layer_field_id
  }, if: proc { layer_field&.field_type == 'float' && layer_field.unique? }
  validates :date_value, uniqueness: {
    case_sensitive: false, scope: :layer_field_id
  }, if: proc { layer_field&.field_type == 'date' && layer_field.unique? }
  validates :time_value, uniqueness: {
    case_sensitive: false, scope: :layer_field_id
  }, if: proc { layer_field&.field_type == 'time' && layer_field.unique? }
  validates :datetime_value, uniqueness: {
    case_sensitive: false, scope: :layer_field_id
  }, if: proc { layer_field&.field_type == 'datetime' && layer_field.unique? }
  validates :layer_field_option_id, uniqueness: {
    case_sensitive: false, scope: :layer_field_id
  }, if: proc { layer_field&.field_type == 'choice' && layer_field.unique? }
  validates :string_value, absence: true, if: proc { layer_field&.field_type != 'string' }
  validates :text_value, absence: true, if: proc { layer_field&.field_type != 'text' }
  validates :integer_value, absence: true, if: proc { layer_field&.field_type != 'integer' }
  validates :float_value, absence: true, if: proc { layer_field&.field_type != 'float' }
  validates :date_value, absence: true, if: proc { layer_field&.field_type != 'date' }
  validates :time_value, absence: true, if: proc { layer_field&.field_type != 'time' }
  validates :datetime_value, absence: true, if: proc { layer_field&.field_type != 'datetime' }
  validates :layer_field_option_id, absence: true, if: proc { layer_field&.field_type != 'choice' }
  validate :validates_editable, on: :update
  validates_associated :layer_field_option

  private

  def validates_editable
    return if layer_field.nil? || layer_field.editable?

    field_types_map = {
      'string' => { db_field: :string_value, condition: string_value_changed? },
      'text' => { db_field: :text_value, condition: text_value_changed? },
      'integer' => { db_field: :integer_value, condition: integer_value_changed? },
      'float' => { db_field: :float_value, condition: float_value_changed? },
      'boolean' => { db_field: :boolean_value, condition: boolean_value_changed? },
      'date' => { db_field: :date_value, condition: date_value_changed? },
      'time' => { db_field: :time_value, condition: time_value_changed? },
      'datetime' => { db_field: :datetime_value, condition: datetime_value_changed? },
      'choice' => { db_field: :layer_field_option_id, condition: layer_field_option_id_changed? }
    }

    return unless field_types_map[layer_field.field_type][:condition]

    errors.add(field_types_map[layer_field.field_type][:db_field])
  end
end
