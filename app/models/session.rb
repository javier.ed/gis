# frozen_string_literal: true

require 'active_record/jwt'

# Session model
class Session < ApplicationRecord
  include ActiveRecord::JWT

  attr_readonly :created_at, :client_app_id, :user_id

  has_jwt :key

  belongs_to :client_app
  belongs_to :user

  validates :finished_at, timeliness: { on_or_before: :now }, allow_blank: true
end
