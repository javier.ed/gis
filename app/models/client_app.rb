# frozen_string_literal: true

require 'active_record/jwt'

# Client application model
class ClientApp < ApplicationRecord
  include ActiveRecord::JWT

  attr_readonly :created_at

  has_jwt :key

  has_many :sessions, dependent: :restrict_with_exception

  validates :description, :scopes, presence: true
  validates :description, length: { maximum: 256 }, allow_blank: true

  def scopes
    self[:scopes]&.split(' ')
  end

  def scopes=(value)
    self[:scopes] = if value.is_a? Array
                      value.join(' ')
                    else
                      value
                    end
  end
end
