# frozen_string_literal: true

# Marker model
class Marker < ApplicationRecord
  has_paper_trail

  attr_accessor :translate

  translates :name, :description, versioning: :paper_trail

  attr_readonly :created_at, :layer_id, :user_id

  belongs_to :layer
  belongs_to :geocoding_point
  belongs_to :user
  has_many :values, class_name: 'MarkerValue', dependent: :destroy, autosave: true
  has_many :approvals, class_name: 'MarkerApproval', dependent: :destroy
  belongs_to :marker_approval, optional: true
  has_many :favorite_markers, dependent: :destroy

  has_one_attached :avatar, dependent: false

  accepts_nested_attributes_for :values

  validates :name, presence: true
  validates :name, length: { maximum: 256 }, allow_blank: true
  validates :avatar, content_type: %i[gif jpeg jpg png]
  validates :start_at, presence: true, if: proc { layer&.start_at_current_time && layer.time_interval != 'disabled' }
  validates :start_at, :finish_at, presence: true, if: proc {
    layer.present? && layer.time_interval != 'any' && layer.time_interval != 'disabled'
  }
  validates :start_at, :finish_at, absence: true, if: proc { layer&.time_interval == 'disabled' }
  validates :start_at, timeliness: { is_at: :time_interval_from_finish_at }, allow_blank: true, if: proc {
    finish_at.present? && layer.present? && layer.time_interval != 'any' && layer.time_interval != 'disabled'
  }
  validates :finish_at, timeliness: { is_at: :time_interval_from_start_at }, allow_blank: true, if: proc {
    start_at.present? && layer.present? && layer.time_interval != 'any' && layer.time_interval != 'disabled'
  }
  validates :start_at, timeliness: { before: :finish_at }, allow_blank: true, if: proc { finish_at.present? }
  validates :finish_at, timeliness: { after: :start_at }, allow_blank: true, if: proc { start_at.present? }
  validate :validate_geocoding_point, if: proc {
    published_at.present? && layer_id.present? && geocoding_point.present?
  }
  validates_associated :values, :geocoding_point

  after_save :generate_translations, if: :translate

  def generate_translations
    TranslateJob.perform_later self, :name, :description
  end

  # @param query [String]
  # @param layer_id [String]
  # @param order [Hash]
  # @param include_unpublished [Boolean]
  # @return [Object]
  def self.search(query, layer_id: nil, order: { id: :desc }, include_unpublished: false)
    markers = joins(
      :translations,
      'INNER JOIN layer_translations ON layer_translations.layer_id = markers.layer_id',
      'INNER JOIN geocoding_point_translations
      ON geocoding_point_translations.geocoding_point_id = markers.geocoding_point_id'
    )
              .where(
                'LOWER(marker_translations.name) LIKE :query OR LOWER(marker_translations.description) LIKE :query OR
                 LOWER(layer_translations.name) LIKE :query OR LOWER(layer_translations.description) LIKE :query OR
                 LOWER(geocoding_point_translations.address) LIKE :query',
                { query: "%#{query&.downcase}%" }
              )
    markers = markers.where(layer_id: layer_id) if layer_id.present?
    markers = markers.where('markers.published_at IS NOT NULL') unless include_unpublished
    markers.order(order).group(:id, *order.keys)
  rescue ArgumentError => e
    logger.error e.message
    logger.error e.backtrace.join("\n")
    nil
  end

  # @param latitude [Float]
  # @param longitude [Float]
  # @param area [Float]
  # @param layer_id [String]
  # @param date_and_time [Time]
  # @param order [Hash]
  # @return [Object]
  def self.locate(latitude, longitude, area: 20, layer_id: nil, date_and_time: nil, order: { id: :desc }) # rubocop:disable Metrics/ParameterLists
    points = GeocodingPoint.joins(:markers).where('markers.published_at IS NOT NULL')
    points = points.where('markers.layer_id = ?', layer_id) if layer_id.present?
    points = points.near([latitude, longitude], area, order: '')
    markers = where(
      'published_at IS NOT NULL AND geocoding_point_id IN (:point_ids)
       AND (DATE_TRUNC(\'second\', start_at) <= DATE_TRUNC(\'second\', :date_and_time::timestamp) OR start_at IS NULL)
       AND (DATE_TRUNC(\'second\', finish_at) > DATE_TRUNC(\'second\', :date_and_time::timestamp)
            OR finish_at IS NULL)',
      { point_ids: points.ids, date_and_time: (date_and_time || Time.current) }
    )
    markers = markers.where(layer_id: layer_id) if layer_id.present?
    markers.order(order)
  rescue ArgumentError => e
    logger.error e.message
    logger.error e.backtrace.join("\n")
    nil
  end

  def unpublish
    return false if published_at.blank?

    update(published_at: nil, marker_approval: nil)
  end

  def build_duplicate(**args)
    new_marker = dup
    new_marker.assign_attributes(
      published_at: nil, marker_approval: nil, avatar: (avatar.attached? ? avatar.blob : nil)
    )
    new_marker.assign_attributes(**args)
    values.each { |value| new_marker.values << value.dup }

    new_marker
  end

  def time_interval_from_finish_at
    finish_at - ChronicDuration.parse(layer.time_interval)
  end

  def time_interval_from_start_at
    start_at + ChronicDuration.parse(layer.time_interval)
  end

  def validate_geocoding_point
    near_markers = Marker.joins(:layer, :geocoding_point)
                         .where(
                           'markers.published_at IS NOT NULL AND markers.layer_id = :layer_id
                           AND ST_Distance(geocoding_points.coordinates, ST_MakePoint(:longitude, :latitude)) <= 5',
                           {
                             layer_id: layer_id, latitude: geocoding_point.latitude,
                             longitude: geocoding_point.longitude
                           }
                         )

    near_markers = near_markers.where('markers.id != ?', id) if id.present?

    if start_at.present?
      near_markers = near_markers.where(
        '(DATE_TRUNC(\'second\', markers.start_at) <= DATE_TRUNC(\'second\', :start_at::timestamp)
          OR markers.start_at IS NULL)
         AND (DATE_TRUNC(\'second\', markers.finish_at) > DATE_TRUNC(\'second\', :start_at::timestamp)
              OR markers.finish_at IS NULL)',
        { start_at: start_at }
      )
    end

    if finish_at.present?
      near_markers = near_markers.where(
        '(DATE_TRUNC(\'second\', markers.start_at) < DATE_TRUNC(\'second\', :finish_at::timestamp)
          OR markers.start_at IS NULL)
         AND (DATE_TRUNC(\'second\', markers.finish_at) >= DATE_TRUNC(\'second\', :finish_at::timestamp)
              OR markers.finish_at IS NULL)',
        { finish_at: finish_at }
      )
    end

    errors.add(:geocoding_point) if near_markers.count.positive?
  end
end
