# frozen_string_literal: true

# Layer field option model
class LayerFieldOption < ApplicationRecord
  has_paper_trail

  attr_accessor :translate

  translates :name, versioning: :paper_trail

  belongs_to :layer_field

  validates :name, presence: true
  validates :name, length: { maximum: 256 }, allow_blank: true
  validates :name, uniqueness: { case_sensitive: false, scope: :layer_field_id }, allow_blank: true

  after_save :generate_translations, if: :translate

  def generate_translations
    TranslateJob.perform_later self, :name
  end
end
