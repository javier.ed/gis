# frozen_string_literal: true

# Favorite marker model
class FavoriteMarker < ApplicationRecord
  has_paper_trail

  attr_readonly :created_at, :user_id, :marker_id

  belongs_to :user
  belongs_to :marker

  validates :marker_id, uniqueness: { scope: :user_id }

  # @param latitude [Float]
  # @param longitude [Float]
  # @param area [Float]
  # @param order [Hash]
  # @return [Object]
  def self.locate(latitude, longitude, area: 20, order: { id: :desc })
    points = GeocodingPoint.joins(:markers).where('markers.published_at IS NOT NULL')
    points = points.near([latitude, longitude], area, order: '')
    joins(:marker).where(
      'markers.published_at IS NOT NULL AND markers.geocoding_point_id IN (?)', points.ids
    ).order(order)
  rescue ArgumentError => e
    logger.error e.message
    logger.error e.backtrace.join("\n")
    nil
  end
end
