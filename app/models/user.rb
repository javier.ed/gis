# frozen_string_literal: true

require 'settings'

# User model
class User < ApplicationRecord
  attr_readonly :created_at, :username

  has_secure_password

  has_many :sessions, dependent: :restrict_with_exception
  has_many :otps, dependent: :restrict_with_exception
  has_many :layers, dependent: :restrict_with_exception
  has_many :markers, dependent: :restrict_with_exception
  has_many :marker_approvals, dependent: :restrict_with_exception
  has_many :favorite_markers, dependent: :restrict_with_exception
  has_many :layer_subscriptions, dependent: :restrict_with_exception
  has_one :profile, dependent: :restrict_with_exception

  validates :username, presence: true
  validates :username, length: { minimum: 5, maximum: 24 }, allow_blank: true
  validates :username, format: { with: /\A[a-z0-9_\-.]+\z/i }, allow_blank: true
  validates :username, exclusion: { in: Settings.username_blacklist }
  validates :username, uniqueness: { case_sensitive: false }
  validates :email, presence: true
  validates :email, length: { minimum: 5, maximum: 256 }, allow_blank: true
  validates :email, format: { with: /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i }, allow_blank: true
  validates :email, uniqueness: { case_sensitive: false }
  validates :password, length: { minimum: 8 }, allow_blank: true
  validates :email_verified_at, timeliness: { on_or_before: :now }, allow_blank: true

  before_save :send_email_verification, if: :email_changed?
  before_create :copy_default_layer_subscriptions
  after_create :send_welcome_mail

  def verify_email(password)
    return false if email_verified_at.present?

    otp = otps.find_and_authenticate('verify_email', password)

    if otp
      update email_verified_at: Time.current
      otp.mark_as_used
      true
    else
      false
    end
  end

  def change_email(new_email)
    if email.casecmp?(new_email)
      errors.add :email
      return false
    end

    update(email: new_email)
  end

  def resend_email_verification
    return false if email_verified_at.present?

    otp = otps.new action: 'verify_email'
    otp.save
  end

  def change_password(new_password, new_password_confirmation = nil)
    if new_password.blank?
      errors.add :password, :blank
      return false
    end

    update password: new_password, password_confirmation: new_password_confirmation
  end

  def self.find_by_login(value)
    find_by(
      'LOWER(username) = :login OR (LOWER(email) = :login AND email_verified_at IS NOT NULL)',
      { login: value.downcase }
    )
  end

  def self.find_by_username(value)
    find_by('LOWER(username) = :username', { username: value.downcase })
  end

  private

  def send_email_verification
    self.email_verified_at = nil

    otps.build(action: 'verify_email')
  end

  def send_welcome_mail
    UserMailer.with(to: email, username: username).welcome_mail.deliver_later
  end

  def copy_default_layer_subscriptions
    LayerSubscription.where('user_id IS NULL').find_each do |s|
      layer_subscriptions.find_or_initialize_by(layer: s.layer)
    end
  end
end
