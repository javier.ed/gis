# frozen_string_literal: true

# Layer subscription
class LayerSubscription < ApplicationRecord
  has_paper_trail

  attr_readonly :created_at, :user_id, :layer_id

  belongs_to :user, optional: true
  belongs_to :layer

  validates :layer_id, uniqueness: { scope: :user_id }

  after_create :copy_to_users

  private

  def copy_to_users
    return if user.present?

    User.find_each do |u|
      u.layer_subscriptions.find_or_create_by(layer: layer)
    end
  end
end
