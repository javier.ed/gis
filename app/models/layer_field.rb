# frozen_string_literal: true

# Layer field model
class LayerField < ApplicationRecord
  has_paper_trail

  attr_accessor :translate

  translates :name, :description, versioning: :paper_trail

  attr_readonly :created_at, :layer_id

  belongs_to :layer

  has_many :options, class_name: 'LayerFieldOption', dependent: :destroy
  has_many :marker_values, dependent: :destroy

  validates :field_type, :name, presence: true
  validates :field_type, inclusion: { in: %w[string text integer float boolean date time datetime choice] },
                         allow_blank: true
  validates :name, length: { maximum: 256 }, allow_blank: true
  validates :name, uniqueness: { case_sensitive: false, scope: :layer_id }, allow_blank: true
  validates_associated :options
  accepts_nested_attributes_for :options

  after_save :generate_translations, if: :translate

  def generate_translations
    TranslateJob.perform_later self, :name, :description
  end
end
