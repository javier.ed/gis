# frozen_string_literal: true

# Marker approval model
class MarkerApproval < ApplicationRecord
  has_paper_trail

  belongs_to :user
  belongs_to :marker

  validates :comment, presence: true, unless: :approved?
  validate :validate_user_is_layer_user
  validate :validate_marker_user_is_not_user
  validate :validate_approved_already

  after_create :publish_marker

  private

  def validate_user_is_layer_user
    return if marker.present? && user&.layers&.find(marker.layer_id)

    errors.add(:user)
  end

  def validate_marker_user_is_not_user
    return if marker.present? && marker.user_id != user_id

    errors.add(:marker)
  end

  def validate_approved_already
    return unless marker&.published_at? && approved?

    errors.add(:marker)
  end

  def publish_marker
    marker.update(published_at: (approved? ? Time.current : nil), marker_approval: self)
  end
end
