# frozen_string_literal: true

# OTP model
class Otp < ApplicationRecord
  attr_readonly :created_at, :action, :password_digest, :user_id

  belongs_to :user
  has_secure_password

  validates :action, inclusion: { in: %w[verify_email reset_password] }
  validates :attempts, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 3 }
  validates :used_at, timeliness: { on_or_before: :now }, allow_blank: true
  validates :discarded_at, timeliness: { on_or_before: :now }, allow_blank: true

  def self.find_and_authenticate(action, password)
    otp = find_by('action = :action AND used_at IS NULL AND discarded_at IS NULL AND attempts < 3', { action: action })
    return false unless otp

    if otp.authenticate(password)
      otp
    else
      otp.update(attempts: otp.attempts + 1)
      false
    end
  end

  def mark_as_used
    update(used_at: Time.current) if used_at.blank?
  end

  after_initialize :generate_password, if: proc { password_digest.blank? }
  after_create :discard_others
  after_create :send_password

  private

  def generate_password
    self.password = SecureRandom.base58(6).upcase
  end

  def discard_others
    user.otps.where(
      'id != :id AND action = :action AND used_at IS NULL AND discarded_at IS NULL AND attempts < 3',
      { id: id, action: action }
    ).update_all(discarded_at: Time.current) # rubocop:disable Rails/SkipsModelValidations
  end

  def send_password
    OtpMailer.with(to: user.email, username: user.username, password: password, action: action).password_mail
             .deliver_later
  end
end
