# frozen_string_literal: true

# Profile model
class Profile < ApplicationRecord
  has_paper_trail

  attr_readonly :created_at, :user_id

  belongs_to :user

  has_one_attached :avatar

  validates :user_id, uniqueness: true
  validates :name, length: { maximum: 256 }
  validates :country_id, inclusion: { in: ISO3166::Country.all.map(&:alpha2) }, allow_blank: true
  validates :birthdate, timeliness: { on_or_before: -> { Date.current }, type: :date }, allow_blank: true
  validates :avatar, content_type: %i[gif jpeg jpg png]

  def country
    ISO3166::Country[country_id]
  end
end
