# frozen_string_literal: true

# Layer model
class Layer < ApplicationRecord
  has_paper_trail

  attr_accessor :translate

  translates :name, :description, versioning: :paper_trail

  attr_readonly :created_at, :user_id

  belongs_to :user

  has_many :fields, class_name: 'LayerField', dependent: :destroy, autosave: true
  has_many :markers, dependent: :destroy
  has_many :subscriptions, class_name: 'LayerSubscription', dependent: :destroy

  validates :name, :description, :time_interval, presence: true
  validates :name, length: { maximum: 256 }, allow_blank: true
  validates :suggestions, inclusion: { in: %w[requires_approval publish_directly deny] }, allow_blank: true
  validates :start_at_current_time, absence: true, if: proc { time_interval == 'disabled' }
  validate :validate_time_interval
  validates_associated :fields
  accepts_nested_attributes_for :fields

  after_save :generate_translations, if: :translate

  def generate_translations
    TranslateJob.perform_later self, :name, :description
  end

  def self.search(query, order: { id: :desc })
    joins(:translations)
      .where(
        'LOWER(layer_translations.name) LIKE :query OR LOWER(layer_translations.description) LIKE :query',
        { query: "%#{query&.downcase}%" }
      ).order(order).group(:id, *order.keys)
  rescue ArgumentError => e
    logger.error e.message
    logger.error e.backtrace.join("\n")
    nil
  end

  def pending_markers
    markers.where('published_at IS NULL AND marker_approval_id IS NULL')
  end

  private

  def validate_time_interval
    return if time_interval.blank? || %w[any disabled].include?(time_interval)

    errors.add(:time_interval) if ChronicDuration.parse(time_interval).nil?
  rescue StandardError => e
    logger.error e.message
    logger.error e.backtrace.join("\n")
    errors.add(:time_interval)
  end
end
