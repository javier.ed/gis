# frozen_string_literal: true

require 'apertium'

# Translate Job
class TranslateJob < ApplicationJob
  queue_as :default

  def perform(resource, *translate)
    default_translation = resource.translations.first

    I18n.available_locales.each do |l|
      next if default_translation.locale == l.to_s || resource.translations.find_by('locale = ?', l).present?

      args = {}

      translate.each do |t|
        next if default_translation[t].blank?

        args[t] = Apertium.translate(default_translation[t], default_translation.locale, l)
      end

      resource.translations.create(locale: l, **args)
    end
  end
end
